!> \file
!> \brief Chemical in-cloud oxidation calculations (for bulk aerosol).
!!
!! @author K. von Salzen
!
!-----------------------------------------------------------------------
subroutine suloxi (dxsiv,dxsvi,dxhp,zhenry,o3frc,h2o2frc,xsiv, &
                         xsvi,xo3,xhp,xna,xam,xss,zclf,zmlwc,ta,rhoa, &
                         co2_ppm,dt,ilga,leva,neqp)
  !
  use sdparm, only : ws,ytiny
  use sdphys, only : rhoh2o,wa
  use fpdef,  only : r8
  !
  implicit none
  !
  integer, intent(in) :: ilga !< Number of grid points in the horizontal direction
  integer, intent(in) :: leva !< Number of grid points in the vertical direction
  integer, intent(in) :: neqp !<
  real, intent(out), dimension(ilga,leva) :: dxsiv !<
  real, intent(out), dimension(ilga,leva) :: dxsvi !<
  real, intent(out), dimension(ilga,leva) :: dxhp !<
  real, intent(out), dimension(ilga,leva) :: zhenry !<
  real, intent(out), dimension(ilga,leva) :: o3frc !<
  real, intent(out), dimension(ilga,leva) :: h2o2frc !<
  real, intent(in), dimension(ilga,leva) :: ta !< Air temperature \f$[K]\f$ 
  real, intent(in), dimension(ilga,leva) :: rhoa !< Air density \f$[kg/m^3]\f$ 
  real, intent(in), dimension(ilga,leva) :: xsiv !<
  real, intent(in), dimension(ilga,leva) :: xsvi !<
  real, intent(in), dimension(ilga,leva) :: xo3 !<
  real, intent(in), dimension(ilga,leva) :: xhp !<
  real, intent(in), dimension(ilga,leva) :: xna !<
  real, intent(in), dimension(ilga,leva) :: xam !<
  real, intent(in), dimension(ilga,leva) :: xss !<
  real, intent(in), dimension(ilga,leva) :: zmlwc !<
  real, intent(in), dimension(ilga,leva) :: zclf !<
  real, intent(in) :: co2_ppm !<
  real, intent(in) :: dt !<
  real(r8), dimension(ilga,leva)         :: ogtso2 !<
  real(r8), dimension(ilga,leva)         :: agtso4 !<
  real(r8), dimension(ilga,leva)         :: agto3 !<
  real(r8), dimension(ilga,leva)         :: agtho2 !<
  real(r8), dimension(ilga,leva)         :: aoh2o2 !<
  real(r8), dimension(ilga,leva)         :: ogthno3 !<
  real(r8), dimension(ilga,leva)         :: ogtnh3 !<
  real(r8), dimension(ilga,leva)         :: ogtco2 !<
  real(r8), dimension(ilga,leva)         :: aresid !<
  real(r8), dimension(ilga,leva)         :: wrk1 !<
  real(r8), dimension(ilga,leva)         :: wrk2 !<
  real(r8), dimension(ilga,leva)         :: antso2 !<
  real(r8), dimension(ilga,leva)         :: antso4 !<
  real(r8), dimension(ilga,leva)         :: antho2 !<
  real(r8), dimension(ilga,leva)         :: alwcvmr !<
  real(r8), dimension(ilga,leva)         :: agtso2 !<
  real(r8), dimension(ilga,leva)         :: agtco2 !<
  real(r8), dimension(ilga,leva)         :: agthno3 !<
  real(r8), dimension(ilga,leva)         :: agtnh3 !<
  real(r8), dimension(ilga,leva)         :: amso3 !<
  real(r8), dimension(ilga,leva)         :: amnh4 !<
  real(r8), dimension(ilga,leva)         :: amno3 !<
  real(r8), dimension(ilga,leva)         :: amso4 !<
  real(r8), dimension(ilga,leva)         :: amssso4 !<
  real(r8), dimension(ilga,leva)         :: amsscl !<
  real(r8), dimension(ilga,leva)         :: amssani !<
  real(r8), dimension(ilga,leva)         :: amsscat !<
  real(r8), dimension(ilga,leva)         :: amcl !<
  real(r8), dimension(ilga,leva)         :: amhcl !<
  real(r8), dimension(ilga,leva)         :: agthcl !<
  real(r8), dimension(ilga,leva)         :: assmr !<
  real(r8), dimension(ilga,leva)         :: aconvf !<
  real(r8), dimension(ilga,leva)         :: adelta !<
  real(r8), dimension(ilga,leva)         :: afoh !<
  real(r8), dimension(ilga,leva)         :: amh !<
  real(r8), dimension(ilga,leva)         :: atmp !<
  real(r8), dimension(ilga,leva)         :: atval !<
  real(r8), dimension(ilga,leva)         :: afras !<
  real(r8), dimension(ilga,leva)         :: afnh4 !<
  real(r8), dimension(ilga,leva)         :: afno3 !<
  real(r8), dimension(ilga,leva)         :: afcl !<
  real(r8), dimension(ilga,leva)         :: afhso !<
  real(r8), dimension(ilga,leva)         :: afco3 !<
  real(r8), dimension(ilga,leva)         :: atvo3 !<
  real(r8), dimension(ilga,leva)         :: af1 !<
  real(r8), dimension(ilga,leva)         :: af2 !<
  real(r8), dimension(ilga,leva)         :: afrah !<
  real(r8), dimension(ilga,leva)         :: apard !<
  real(r8), dimension(ilga,leva)         :: apara !<
  real(r8), dimension(ilga,leva)         :: aparb !<
  real(r8), dimension(ilga,leva)         :: aparc !<
  real(r8), dimension(ilga,leva)         :: atvalx !<
  real(r8), dimension(ilga,leva)         :: asrso2 !<
  real(r8), dimension(ilga,leva)         :: adeltax !<
  real(r8), dimension(ilga,leva)         :: xsivt !<
  real(r8), dimension(ilga,leva)         :: wrktot !<
  real(r8), dimension(ilga,leva,neqp) :: achpa !<
  real, parameter :: ycom3l = 1.e+03_r8 !<
  real, parameter :: zsec   = 1.e-07_r8 !<
  real(r8), parameter :: amhmin = 1.e-10_r8 !<
  real(r8), parameter :: amhmax = 1.e-01_r8 !<
  integer, parameter :: ysub=2 !<
  real, parameter :: atunex=1. !<
  integer :: ilgas !<
  integer :: indti !<
  integer :: levi !<
  real :: atimst !<
  !
  !-----------------------------------------------------------------------
  !     * initializations of parameters.
  !
  afoh=1.e-14_r8
  dxsiv =0.
  dxsvi =0.
  dxhp  =0.
  zhenry=0.
  aresid=0.
  wrk1  =0.
  wrk2  =0.
  atimst=dt/real(ysub)
  !
  !     * HENRY'S LAW CONSTANTS AND REACTION RATES.
  !
  levi=1
  ilgas=1
  call equip(ilga,leva,neqp,levi,ilgas,ilga,achpa,ta)
  !
  !     * conversion of units to mol/litre for following input units:
  !     * so2: kg-s/kg-air
  !     * so4: kg-s/kg-air
  !     * h2o2: kg-s/kg-air
  !     * o3, hno3, nh3, co2: volume mixing ratio
  !
  ogtso2 =xsiv*rhoa/(ws*ycom3l)
  agtso4 =xsvi*rhoa/(ws*ycom3l)
  agto3  =xo3*rhoa/(wa*ycom3l)
  agtho2 =xhp*rhoa/(ws*ycom3l)
  ogthno3=xna*rhoa/(wa*ycom3l)
  ogtnh3 =xam*rhoa/(wa*ycom3l)
  ogtco2 =co2_ppm*rhoa/(wa*ycom3l)
  !
  !     * save input.
  !
  aoh2o2=agtho2
  !
  !-----------------------------------------------------------------------
  !     * s(iv) oxidation. chemical sources/sinks are considered
  !     * for sulphur dioxide, hydrogen peroxide, ozone. they
  !     * are only applied if all of these three tracers plus
  !     * sulphate are available. the corresponding sulphate
  !     * production rate is given by af1*[o3]*[so2]+af2*[h2o2]*[so2]
  !     * (in kg-sulphur/kg-air/sec), where the concentrations
  !     * refer to the total (gas phase + dissolved) species (in
  !     * kg-sulphur/kg-air). the internal time step in these
  !     * calculations is given by: time_step(model)/ysub.
  !
  do indti = 1, ysub
    where (zmlwc > zsec .and. zclf > zsec)
      !
      !         * initialization.
      !
      antso2=ogtso2
      antho2=agtho2
      antso4=agtso4
      !
      !         * liquid water volume mixing ratio.
      !
      alwcvmr=zmlwc*rhoa/rhoh2o
      !
      !         * total (aqueous + gas-phase) concentrations.
      !
      agtso2 = ogtso2
      agtco2 = ogtco2
      agthno3= ogthno3
      agtnh3 = ogtnh3
      !
      !         * molarities.
      !
      amso3= 0._r8
      amnh4=agtnh3 /alwcvmr
      amno3=agthno3/alwcvmr
      amso4=agtso4 /alwcvmr
      !
      !         * sea salt species.
      !
      assmr=xss
      aconvf=rhoa/(alwcvmr*ycom3l)
      !
      !         * calculate molarities of sea salt species assuming a typical
      !         * composition of sea water. no anions other than sulphate and
      !         * chloride are considered. all cations, except h+, are lumped
      !         * together. the ph of the sea water is assumed to be 8.2.
      !         * h+ is accounted for in the ion balance and in the calculation
      !         * of hydrogen chlorine in the gas phase.
      !
      amssso4=0.077_r8*aconvf/96.058e-03_r8*assmr
      amsscl =0.552_r8*aconvf/35.453e-03_r8*assmr
      amssani=0._r8
      amsscat=(amsscl+2._r8*amssso4+amssani)*(1._r8-1.048e-08_r8)
      amso4=amso4+amssso4
      amcl=amsscl
      amhcl=amsscl*6.31e-09_r8/achpa(:,:,10)
      agthcl=(amsscl+amhcl)*alwcvmr
      adelta=amno3+2._r8*(amso4+amso3)+amcl+amssani-amnh4-amsscat
      atmp=adelta**2+4._r8*(afoh &
                              +achpa(:,:,2)*agtso2+achpa(:,:,5)*agtco2)
    else where
      atmp=0._r8
    end where
    !
    !       * initial estimate of proton molarity from initial
    !       * molarities.
    !
    amh=sqrt(atmp)
    amh=max(min(amh,amhmax),amhmin)
    where (zmlwc > zsec .and. zclf > zsec)
      !
      !         * equilibrium parameters for soluble species.
      !
      atval=achpa(:,:,1)+achpa(:,:,2)/amh+achpa(:,:,3)/amh**2
      afras=1._r8/( 1._r8+alwcvmr*atval)
      afnh4=1._r8/( 1._r8+agtnh3/(1._r8/achpa(:,:,9)+alwcvmr*amh) )
      afno3=agthno3/( 1._r8/achpa(:,:,8)+alwcvmr/amh)
      !
      !         * first iteration in proton molarity calculation.
      !
      afcl =agthcl/( 1._r8/achpa(:,:,10)+alwcvmr/amh)
      afhso=agtso2*afras*achpa(:,:,2)
      afco3=agtco2*achpa(:,:,5)
      amso3=( agtso2*afras*achpa(:,:,3) )/amh**2
      adelta=afnh4*( 2._r8*(amso4+amso3)+amssani-amsscat)
      amh=0.5_r8*( adelta+ &! proc-depend
                              sqrt(adelta**2+4._r8*afnh4*(afoh &
                                            +afco3+afhso+afno3+afcl)) )
      amh=max(min(amh,amhmax),amhmin)
      atval=achpa(:,:,1)+achpa(:,:,2)/amh+achpa(:,:,3)/amh**2
      afras=1._r8/( 1._r8+alwcvmr*atval)
      afnh4=1._r8/( 1._r8+agtnh3/(1._r8/achpa(:,:,9)+alwcvmr*amh) )
      afno3=agthno3/( 1._r8/achpa(:,:,8)+alwcvmr/amh)
      !
      !         * second iteration in proton molarity calculation.
      !
      afcl =agthcl/( 1._r8/achpa(:,:,10)+alwcvmr/amh)
      afhso=agtso2*afras*achpa(:,:,2)
      afco3=agtco2*achpa(:,:,5)
      amso3=( agtso2*afras*achpa(:,:,3) )/amh**2
      adelta=afnh4*( 2._r8*(amso4+amso3)+amssani-amsscat)
      amh=0.5_r8*( adelta+ &! proc-depend
                              sqrt(adelta**2+4._r8*afnh4*(afoh &
                                            +afco3+afhso+afno3+afcl)) )
      amh=max(min(amh,amhmax),amhmin)
      atval=achpa(:,:,1)+achpa(:,:,2)/amh+achpa(:,:,3)/amh**2
      afras=1._r8/( 1._r8+alwcvmr*atval)
      !
      !         * ozone oxidation parameter (in 1/sec).
      !
      atvo3=1._r8/( 1._r8+alwcvmr*achpa(:,:,7) )
      af1=( achpa(:,:,11)+achpa(:,:,12)/amh)*afras &
                                      *atval*atvo3*achpa(:,:,7)*alwcvmr
      af1=af1*agto3
      af1=af1*atunex
      !
      !         * hydrogen peroxide oxidation parameter (in litre-air/mol/sec).
      !
      afrah=1._r8/( 1._r8+alwcvmr*achpa(:,:,6) )
      af2=( achpa(:,:,13)/(0.1_r8+amh) )*afras &
                               *achpa(:,:,1)*afrah*achpa(:,:,6)*alwcvmr
      af2=af2*atunex
      !
      !         * semi-implicit integration.
      !
      apard=agtho2
      apara=( 1._r8+atimst*af1)*atimst*af2
      aparc=-agtso2
      aparb=1._r8+atimst*( af1+af2*(apard+aparc) )
      atvalx=-aparb/( 2._r8*apara)
      atmp=atvalx**2-aparc/apara
    else where
      atmp=0._r8
    end where
    antso2=sqrt(atmp)
    where (zmlwc > zsec .and. zclf > zsec)
      antso2=atvalx+antso2
      antso2=max(antso2,0._r8)
      antso2=min(antso2,agtso2)
      antho2=apard/(1._r8+atimst*af2*antso2)
      antho2=max(antho2,0._r8)
      antho2=min(antho2,agtho2)
      adelta=agtso2-antso2
      antso4=agtso4+adelta
      antso4=max(antso4,0._r8)
      wrk1  =wrk1+af1/real(ysub)
      wrk2  =wrk2+af2*agtho2/real(ysub)
      !
      !         * scavenging ratios, i.e. fraction of tracer that
      !         * is dissolved. only for so2 so far.
      !
      asrso2=max(1._r8-afras,0._r8)
      zhenry=zhenry+asrso2/real(ysub)
      !
      !         * update fields.
      !
      adelta=ogtso2-antso2
      aresid=aresid+adelta
      !
      !         * final concentrations after atimst.
      !
      ogtso2=antso2
      agtho2=antho2
      agtso4=antso4
    end where
  end do
  adeltax=aresid*ycom3l*ws/rhoa
  adeltax=max(adeltax,0._r8)
  xsivt=xsiv
  adeltax=min(xsivt,adeltax)
  !
  !-----------------------------------------------------------------------
  !     * sulphur dioxide.
  !
  dxsiv=-adeltax
  !
  !     * sulphate.
  !
  dxsvi=adeltax
  !
  !     * hydrogen peroxide.
  !
  adeltax=(agtho2-aoh2o2)*ycom3l*ws/rhoa
  dxhp=adeltax
  !
  !     * ozone and hydrogen peroxide oxidation rates.
  !
  wrktot=wrk1+wrk2
  where (wrktot > ytiny)
    o3frc=min(max(wrk1/wrktot,0._r8),1._r8)*adeltax/dt
    h2o2frc=min(max(wrk2/wrktot,0._r8),1._r8)*adeltax/dt
  else where
    o3frc=0._r8
    h2o2frc=0._r8
  end where
  !
end subroutine suloxi
