!> \file
!> \brief Extracts characteristic non-dimensionalized paricle growth curves
!>       and limits for given initial particle size, supersaturation, and
!>       Koehler parameters.
!!
!! @author K. von Salzen
!
!-----------------------------------------------------------------------
subroutine cnextr (bsp,xp,xps,tps,kop,irs,irsmax,irds,irgs,ibs, &
                         ilga,leva,isec,irstmax)
  !
  use cnparm, only : idef,idpd,rgpn,tpder,tppnt,xpder,xppnt
  !     use ieee_arithmetic, only: ieee_is_finite
  !
  implicit none
  !
  integer, intent(in) :: ilga !<
  integer, intent(in) :: leva !<
  integer, intent(in) :: isec !<
  integer, intent(in) :: irstmax !<
  integer, intent(in), dimension(ilga,leva,isec) :: irds !<
  integer, intent(in), dimension(ilga,leva,isec) :: irgs !<
  integer, intent(in), dimension(ilga,leva,isec) :: ibs !<
  integer, intent(in), dimension(ilga,leva,isec) :: irs !<
  real, intent(in), dimension(ilga,leva,isec) :: bsp !<
  integer, intent(in), dimension(leva,isec) :: irsmax !<
  real, intent(inout), dimension(ilga,leva,isec) :: xp !<
  real, intent(out), dimension(ilga,leva,isec,irstmax) :: xps !<
  real, intent(out), dimension(ilga,leva,isec,irstmax) :: tps !<
  logical, intent(out), dimension(ilga,leva,isec) :: kop !<
  integer, dimension(ilga) :: irdsl !<
  integer, dimension(ilga) :: irgl !<
  integer, dimension(ilga) :: ibsl !<
  integer, dimension(ilga) :: irsl !<
  real, dimension(ilga) :: bspl !<
  integer :: l !<
  integer :: is !<
  integer :: il !<
  integer :: ir !<
  integer :: irgst !<
  integer :: irdst !<
  integer :: ibstt !<
  integer :: id !<
  integer :: irst !<
  !
  !-----------------------------------------------------------------------
  kop(:,:,:)=.false.
  do is=1,isec
    do l=1,leva
      !
      !       * interpolate tabulated data to obtain growth curve that
      !       * includes xp for given bs-parameter. xp is the scaled
      !       * particle size and tp the scaled growth time. pnt and der
      !       * refer to the intial point and derivative with respect to bs,
      !       * respectively. indices ir... and ib... refer to the koehler
      !       * parameter ratio- and bs-axis of the data, with bs the
      !       * scaled b-parameter in the koehler equation.
      !
      irdsl(:)=irds(:,l,is)
      irgl (:)=irgs(:,l,is)
      bspl (:)=bsp (:,l,is)
      ibsl (:)=ibs (:,l,is)
      irsl (:)=irs (:,l,is)
      do ir=1,irsmax(l,is)
        do il=1,ilga
          irgst=irgl(il)
          if (irgst /= idef .and. ir <= irsl(il) ) then
            irdst=irdsl(il)
            ibstt=ibsl(il)
            id=idpd(irgst,irdst,ibstt,ir)
            xps(il,l,is,ir)=xppnt(id)+bspl(il)*xpder(id)
            ! if (.not. ieee_is_finite(xps(il,l,is,ir))) then
            ! print '("CNEXTR:XPS(IL,L,IS,IR):",4es12.4,4i4)', XPS(IL,L,IS,IR),XPPNT(ID),BSPL(IL),XPDER(ID), IL,L,IS,IR
            ! end if
            tps(il,l,is,ir)=tppnt(id)+bspl(il)*tpder(id)
          end if
        end do
      end do
      !
      !       * save flag to indicate whether the upper boundary of the
      !       * domain is open to further particle growth.
      !
      do il=1,ilga
        irgst=irgl(il)
        if (irgst /= idef) then
          irdst=irdsl(il)
          ibstt=ibsl(il)
          kop(il,l,is)=rgpn(irgst)%kopen(ibstt,irdst)
        end if
      end do
      !
      !       * make sure particle size is not smaller than smallest particle
      !       * size according to the growth data. this could happen because
      !       * of truncation errors.
      !
      do il=1,ilga
        irst=irsl(il)
        if (irst > 0) then
          xp(il,l,is)=max(xp(il,l,is),xps(il,l,is,1))
        end if
      end do
    end do
  end do
  !
end subroutine cnextr
