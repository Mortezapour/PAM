!> \file
!> \brief Transformation of number and mass to PLA parameters, including
!>       calculation of aerosol density for internally mixed aerosol and
!>       mass and number corrections, if necessary.
!!
!! @author K. von Salzen
!
!-----------------------------------------------------------------------
subroutine nm2par (pen0,pephi0,pepsi,pephis0,pedphi0,pin0,piphi0, &
                         pipsi,piphis0,pidphi0,piddn,penum,pemas,peddn, &
                         pinum,pimas,pifrc,ilga,leva)
  !
  use sdparm, only : isaext,isaint,kint,kpext,kpint,pedphis,peismax, &
                         peismin,pephiss,pext,pidphis,piismax,piismin,piphiss
  !
  implicit none
  !
  integer, intent(in) :: ilga !< Number of grid points in the horizontal direction
  integer, intent(in) :: leva !< Number of grid points in the vertical direction
  real, intent(inout), dimension(ilga,leva,isaint) :: pimas !< Aerosol (dry) mass concentration for internally mixed
  !< aerosol \f$[kg/kg]\f$
  real, intent(inout), dimension(ilga,leva,isaint) :: pinum !< Aerosol number concentration for internally mixed
  !< aerosol \f$[1/kg]\f$
  real, intent(out), dimension(ilga,leva,isaint) :: pin0 !< 1st PLA size distribution parameter (\f$n_{0,i}\f$,
  !< amplitude) int. mixture
  real, intent(out), dimension(ilga,leva,isaint) :: piphi0 !< 3rd pla size distribution parameter (\f$\phi_{0,i}\f$,
  !< width), int. mixture
  real, intent(out), dimension(ilga,leva,isaint) :: pipsi !< 2nd pla size distribution parameter (\f$\psi_{i}\f$,
  !< mode size) int. mixture
  real, intent(out), dimension(ilga,leva,isaint) :: piddn !< (Dry) density for internally mixed aerosol particles
  !< \f$[kg/m^3]\f$
  real, intent(in), dimension(ilga,leva,isaint,kint) :: pifrc !< Aerosol (dry) mass fraction for each aerosol type
  !< for internally mixed aerosol
  real, intent(in),  dimension(ilga,leva,isaint) :: pidphi0 !< Section size parameter for internally mixed aerosol
  real, intent(in),  dimension(ilga,leva,isaint) :: piphis0 !< Dry particle size (\f$ln(R_p /R_0)\f$) at the boundaries of
  !< the size sections for internally mixed aerosol
  real, intent(in), dimension(ilga,leva,isaext) :: peddn !< (Dry) density for externally mixed aerosol particles
  !< \f$[kg/m^3]\f$
  real, intent(inout), dimension(ilga,leva,isaext) :: pemas !< Aerosol (dry) mass concentration for externally mixed
  !< aerosol \f$[kg/kg]\f$
  real, intent(inout), dimension(ilga,leva,isaext) :: penum !< Aerosol number concentration for externally mixed
  !< aerosol \f$[1/kg]\f$
  real, intent(out), dimension(ilga,leva,isaext) :: pen0 !< 1st PLA size distribution parameter (\f$n_{0,i}\f$,
  !< amplitude) ext. mixture
  real, intent(out), dimension(ilga,leva,isaext) :: pephi0 !< 3rd pla size distribution parameter (\f$\phi_{0,i}\f$,
  !< width), ext. mixture
  real, intent(out), dimension(ilga,leva,isaext) :: pepsi !< 2nd pla size distribution parameter (\f$\psi_{i}\f$,
  !< mode size) ext. mixture
  real, intent(in),  dimension(ilga,leva,isaext) :: pedphi0 !< Section size parameter for externally mixed aerosol
  real, intent(in),  dimension(ilga,leva,isaext) :: pephis0 !< Dry particle size (\f$ln(R_p /R_0)\f$) at the boundaries of
  !< the size sections for externally mixed aerosol
  !
  !     internal work variables
  !
  real, dimension(ilga,leva) :: corn !<
  real, dimension(ilga,leva) :: corm !<
  real, dimension(ilga,leva) :: resn !<
  real, dimension(ilga,leva) :: resm !<
  real, dimension(ilga,leva) :: cornt !<
  real, dimension(ilga,leva) :: cormt !<
  real, dimension(ilga,leva) :: resnt !<
  real, dimension(ilga,leva) :: resmt !<
  !
  !-----------------------------------------------------------------------
  !     * update dry particle density (internally mixed aerosol).
  !
  if (isaint > 0) call sddens(piddn,pifrc,ilga,leva)
  !
  !-----------------------------------------------------------------------
  !     * initializations.
  !
  corn=0.
  corm=0.
  resn=0.
  resm=0.
  !
  !-----------------------------------------------------------------------
  !     * aerosol number and mass adjustments.
  !
  if (isaext > 0) then
    call cornmi(resmt,resnt,cormt,cornt,penum,pemas,peddn, &
                    peismin,peismax,pephiss,pedphis,ilga,leva,isaext)
    corn=corn+cornt
    corm=corm+cormt
    resn=resn+resnt
    resm=resm+resmt
  end if
  if (isaint > 0) then
    call cornmi(resmt,resnt,cormt,cornt,pinum,pimas,piddn, &
                    piismin,piismax,piphiss,pidphis,ilga,leva,isaint)
    corn=corn+cornt
    corm=corm+cormt
    resn=resn+resnt
    resm=resm+resmt
  end if
  !
  !     * update basic pla parameters.
  !
  if (isaext > 0) then
    call nm2pla(pen0,pephi0,pepsi,resn,resm,penum,pemas, &
                    peddn,pephiss,pedphis,pephis0,pedphi0, &
                    ilga,leva,isaext,kpext)
  end if
  if (isaint > 0) then
    call nm2pla(pin0,piphi0,pipsi,resn,resm,pinum,pimas, &
                    piddn,piphiss,pidphis,piphis0,pidphi0, &
                    ilga,leva,isaint,kpint)
  end if
  !
end subroutine nm2par
