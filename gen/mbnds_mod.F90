!> \file
!> \brief Basic arrays for characterization of left and right boundaries
!>       for some data structures.
!!
!! @author K. von Salzen
!
!-----------------------------------------------------------------------
module mbnds
  !
  implicit none
  !
  type bnds
    real :: vl !<
    real :: vr !<
  end type bnds
  type bndsp
    real, pointer :: vl !<
    real, pointer :: vr !<
  end type bndsp
  !
end module mbnds
