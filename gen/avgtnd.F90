!> \file
!> \brief Time-averaged tendencies for internally mixed aerosol species.
!>       Instantaneously calculated tendencies are averaged and saved.
!>       Results are subsequently scaled to allow updates to pam tracers
!>       on a per-time-step basis.
!!
!! @author K. von Salzen
!
!-----------------------------------------------------------------------
subroutine avgtnd(scf,pidndt,pidmdt,pidfdt,pnvb,psvb,pncb, &
                        pscb,pnmb,psmb,pinum,pimas,pifrc,dt, &
                        kount,ilga,leva)
  !
  use sdparm, only : ytiny,isaint,yna,kint,ysmall
  use compar, only : icfrq,iavgprd,iupdatp,nrmfld
  !
  implicit none
  !
  integer, intent(in) :: ilga !< Number of grid points in the horizontal direction
  integer, intent(in) :: leva !< Number of grid points in the vertical direction
  real, intent(in) :: dt !< Model time step \f$[s]\f$
  integer, intent(in) :: kount !< Time step number
  real, intent(inout), dimension(ilga,leva) :: scf !< Scaling factor
  real, intent(inout), dimension(ilga,leva,isaint) :: pidmdt !< Particle mass coagulation tendency \f$[kg/kg/sec]\f$, internal mixture
  real, intent(inout), dimension(ilga,leva,isaint) :: pidndt !< Particle number coagulation tendency \f$[kg/kg/sec]\f$, internal mixture
  real, intent(inout), dimension(ilga,leva,isaint,kint) :: pidfdt !< Aerosol species mass fraction tendency \f$[1/sec]\f$, internal mixture
  real, intent(inout), dimension(ilga,leva,isaint) :: pnvb !< PLA internal array
  real, intent(inout), dimension(ilga,leva,isaint,nrmfld) :: pnmb !< PLA internal array
  real, intent(inout), dimension(ilga,leva,isaint,kint) :: psvb !< PLA internal array
  real, intent(inout), dimension(ilga,leva,isaint,kint,nrmfld) :: psmb !< PLA internal array
  integer, intent(inout), dimension(ilga,leva,isaint,nrmfld) :: pncb !< PLA internal array
  integer, intent(inout), dimension(ilga,leva,isaint,kint,nrmfld) :: pscb !< PLA internal array
  real, intent(in), dimension(ilga,leva,isaint) :: pinum !< Aerosol number concentration for internally mixed aerosol \f$[1/kg]\f$
  real, intent(in), dimension(ilga,leva,isaint) :: pimas !< Aerosol (dry) mass concentration for internally mixed aerosol \f$[kg/kg]\f$
  real, intent(in), dimension(ilga,leva,isaint,kint) :: pifrc !< Aerosol (dry) mass fraction for each aerosol type
  !
  real, dimension(ilga,leva,isaint) :: pnvm !<
  real, dimension(ilga,leva,isaint,kint) :: psvm !<
  real, dimension(ilga,leva,isaint,nrmfld) :: pnmm !<
  real, dimension(ilga,leva,isaint,kint,nrmfld) :: psmm !<
  real, allocatable, dimension(:,:,:) :: pimast !<
  real, allocatable, dimension(:,:,:,:) :: pifrct !<
  real, allocatable, dimension(:,:,:,:) :: pidsdt !<
  real, allocatable, dimension(:,:,:,:) :: pisast !<
  integer, dimension(ilga,leva,isaint,nrmfld) :: pncm !<
  integer, dimension(ilga,leva,isaint,kint,nrmfld) :: pscm !<
  integer :: ind !<
  integer :: is !<
  integer :: k !<
  integer :: ioff !<
  integer :: nrmfldl !<
  !
  !-----------------------------------------------------------------------
  !
  if (isaint > 0) then
    allocate(pimast (ilga,leva,isaint))
    allocate(pifrct (ilga,leva,isaint,kint))
    allocate(pidsdt (ilga,leva,isaint,kint))
    allocate(pisast (ilga,leva,isaint,kint))
  end if
  !
  if (mod(kount,icfrq) == 0) then
    !
    !       * aerosol mass tendencies for individual species.
    !
    pimast=max(pimas+dt*pidmdt,0.)
    pifrct=max(pifrc+dt*pidfdt,0.)
    do k=1,kint
      where (abs(pimas(:,:,:)-yna) > ytiny)
        pidsdt(:,:,:,k)=(pifrct(:,:,:,k)*pimast(:,:,:) &
                            -pifrc (:,:,:,k)*pimas (:,:,:))/dt
      else where
        pidsdt(:,:,:,k)=yna
      end where
    end do
  else
    pidndt=yna
    pidsdt=yna
  end if
  !
  !     * retrieve mean and accumulated result and counter from
  !     * previous time step.
  !
  pnvm=pnvb
  psvm=psvb
  !
  pncm=pncb
  pscm=pscb
  !
  pnmm=pnmb
  psmm=psmb
  !
  !     * loop over all fields involved in calculation of running
  !     * mean results.
  !
  nrmfldl=ceiling(real(iavgprd)/real(iupdatp))
  do ind=1,nrmfldl
    ioff=(kount+iavgprd-1)-(ind-1)*iupdatp
    if (ioff >= 1 .and. mod(ioff,iavgprd)==0) then
      !
      !         * running mean tendencies.
      !
      where (pncm(:,:,:,ind) > 0)
        pnvm(:,:,:)=pnmm(:,:,:,ind)/real(pncm(:,:,:,ind))
      else where
        pnvm(:,:,:)=yna
      end where
      !
      where (pscm(:,:,:,:,ind) > 0)
        psvm(:,:,:,:)=psmm(:,:,:,:,ind)/real(pscm(:,:,:,:,ind))
      else where
        psvm(:,:,:,:)=yna
      end where
      !
      !         * reset accumulated result and counter.
      !
      pnmm(:,:,:,  ind)=0.
      psmm(:,:,:,:,ind)=0.
      !
      pncm(:,:,:,  ind)=0
      pscm(:,:,:,:,ind)=0
    end if
    !
    !       * accumulate instantaneous result and advance counter.
    !
    where (abs(pidndt(:,:,:)-yna) > ytiny)
      pnmm(:,:,:,ind)=pnmm(:,:,:,ind)+pidndt(:,:,:)
      pncm(:,:,:,ind)=pncm(:,:,:,ind)+1
    end where
    !
    where (abs(pidsdt(:,:,:,:)-yna) > ytiny)
      psmm(:,:,:,:,ind)=psmm(:,:,:,:,ind)+pidsdt(:,:,:,:)
      pscm(:,:,:,:,ind)=pscm(:,:,:,:,ind)+1
    end where
  end do
  !
  !     * save mean and accumulated results and counter.
  !
  pnvb=pnvm
  psvb=psvm
  !
  pncb=pncm
  pscb=pscm
  !
  pnmb=pnmm
  psmb=psmm
  !
  !     * restore tendencies to mean values.
  !
  pidndt=pnvm
  pidsdt=psvm
  !
  !     * scale tendencies to avoid negative concentrations,
  !     * if necessary
  !
  do is=1,isaint
    do k=1,kint
      where (abs(pimas(:,:,is)-yna) > ytiny &
          .and. pidsdt(:,:,is,k) < -ysmall)
        scf(:,:)=min(scf(:,:),max(0., &
                 -pifrc(:,:,is,k)*pimas(:,:,is)/(dt*pidsdt(:,:,is,k))))
      end where
    end do
    where (abs(pinum(:,:,is)-yna) > ytiny &
        .and. pidndt(:,:,is) < -ysmall)
      scf(:,:)=min(scf(:,:), &
                            max(0.,-pinum(:,:,is)/(dt*pidndt(:,:,is))))
    end where
  end do
  do is=1,isaint
    do k=1,kint
      where (abs(pimas(:,:,is)-yna) > ytiny)
        pidsdt(:,:,is,k)=scf(:,:)*pidsdt(:,:,is,k)
        pisast(:,:,is,k)=max(pifrc(:,:,is,k)*pimas(:,:,is) &
                                               +pidsdt(:,:,is,k)*dt,0.)
      end where
    end do
    where (abs(pinum(:,:,is)-yna) > ytiny)
      pidndt(:,:,is)=scf(:,:)*pidndt(:,:,is)
    else where
      pidndt(:,:,is)=0.
    end where
  end do
  where (abs(pimas(:,:,:)-yna) > ytiny)
    pimast(:,:,:)=sum(pisast(:,:,:,:),dim=4)
    pidmdt(:,:,:)=(pimast(:,:,:)-pimas(:,:,:))/dt
  else where
    pidmdt(:,:,:)=0.
  end where
  do k=1,kint
    where (abs(pimas(:,:,:)-yna) > ytiny &
        .and. abs(pimast(:,:,:)) > ysmall)
      pidfdt(:,:,:,k)=(pisast(:,:,:,k)/pimast(:,:,:) &
                                                    -pifrc(:,:,:,k))/dt
    else where
      pidfdt(:,:,:,k)=0.
    end where
  end do
  !
  if (isaint > 0) then
    deallocate(pimast)
    deallocate(pifrct)
    deallocate(pidsdt)
    deallocate(pisast)
  end if
  !
end subroutine avgtnd
