!> \file
!> \brief Calculation of dry aerosol density for internally mixed aerosol.
!!
!! @author K. von Salzen
!
!-----------------------------------------------------------------------
subroutine sddens(piddn,pifrc,ilga,leva)
  !
  use sdparm, only : aintf,isaint,kint,sintf,yna,ytiny
  !
  implicit none
  !
  integer, intent(in) :: ilga !< Number of grid points in the horizontal direction
  integer, intent(in) :: leva !< Number of grid points in the vertical direction
  real, intent(in), dimension(ilga,leva,isaint,kint) :: pifrc !< Aerosol (dry) mass fraction for each aerosol type
  !< for internally mixed aerosol
  real, intent(out), dimension(ilga,leva,isaint) :: piddn !< (Dry) density for internally mixed aerosol particles
  !< \f$[kg/m^3]\f$
  !
  !     internal work variables
  !
  integer :: is !<
  integer :: kxt !<
  integer :: kx !<
  integer :: isi !<
  real, allocatable, dimension(:,:,:) :: dm0 !<
  real, allocatable, dimension(:,:,:) :: dv0 !<
  !
  !-----------------------------------------------------------------------
  !
  if (isaint > 0) then
    !
    !       * allocate and initialize work arrays.
    !
    allocate(dm0(ilga,leva,isaint))
    allocate(dv0(ilga,leva,isaint))
    dm0=0.
    dv0=0.
    !
    !       * aerosol masses and volumes in each section.
    !
    do is=1,isaint
      do kxt=1,sintf%isaer(is)%itypt
        kx=sintf%isaer(is)%ityp(kxt)
        isi=sintf%isaer(is)%isi
        dm0(:,:,is)=dm0(:,:,is)+pifrc(:,:,isi,kx)
        dv0(:,:,is)=dv0(:,:,is)+pifrc(:,:,isi,kx)/aintf%tp(kx)%dens
      end do
    end do
    piddn=yna
    where (2+abs(exponent(dm0) - exponent(dv0)) &
        < maxexponent(dm0) .and. dv0/=0. )
      piddn=dm0/dv0
    end where
    !
    !       * deallocate work arrays.
    !
    deallocate(dm0)
    deallocate(dv0)
  end if
  !
end subroutine sddens
!> \file
!! \subsection ssec_details Details
!! SECONDARY INPUT/OUTPUT VARIABLES (VIA MODULES AND COMMON BLOCKS):
!! |In/Out | Name   | Details                                                  |
!! |:------|:-------|:---------------------------------------------------------|
!! | I     | AINTF%TP(:)%DENS | DRY PARTICLE DENSITY (KG/M3), INT. MIXTURE|
!! | I     | ISAINT |  NUMBER OF AEROSOL TRACERS, INT. MIXTURE|
!! | I     | KINT   |  NUMBER OF INTERNALLY MIXED AEROSOL TYPES, INT. MIXTURE|
!! | I     | SINTF%ISAER(:)%ISI | AEROSOL TRACER INDEX, INT. MIXTURE|
!! | I     | SINTF%ISAER(:)%ITYP(:) | AEROSOL TYPE INDEX, INT. MIXTURE|
!! | I     | SINTF%ISAER(:)%ITYPT | NUMBER OF AEROSOL TYPES, INT. MIXTURE|
!! | I     | YTINY  |  SMALLEST VALID NUMBER|
