!> \file
!> \brief Indices for gas-phase tracers
!!
!! @author S. Hanna
!
!-----------------------------------------------------------------------
module trcind
  !
  implicit none
  !
  integer :: iso2 !< index for SO2
  integer :: ihpo !< index for H2O2
  integer :: igs6 !< index for H2SO4
  integer :: igsp !< index for SOAP (secondary organic aerosol precursor gas)
  integer :: idms !< index for DMS
  !
end module trcind
