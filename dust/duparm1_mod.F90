!> \file
!> \brief Basic constants and arrays for dust emission.
!!
!! @authors K. von Salzen, M. Lazare, and Y. Peng
!
!-----------------------------------------------------------------------
module duparm1
  !
  implicit none
  !
  !       * array dimensions
  !
  integer, parameter :: ntrace=8 !< Number of tracers
  integer, parameter :: nbin=24 !< Number of bins per tracer
  integer, parameter :: nclass=ntrace*nbin !< Number of particle classes
  integer, parameter :: nats=17 !< Number of soil types
  integer, parameter :: nmode=4 !< Number of soil population modes
  integer, parameter :: nspe=nmode*3+2 !< NMODE*3+2
  !
  !       size indices for accumulations and coarse modes /cutoff radius between ai and ci is 0.68um, cutoff
  !
  integer, parameter :: minai = 1 !< Min size index for accumulation mode
  integer, parameter :: maxai = 3 !< Max size index for accumulation mode
  integer, parameter :: minci = 4 !< Min size index for coarse mode
  integer, parameter :: maxci = 6 !< Max size index for coarse mode
  !
  !==================================================================
  ! physical (adjustable) parameters
  !     * tuning parameter for pla dust
  !
  real, parameter :: cuscpla = 1.25 !< scale factor for wind stress threshold
  !
!==================================================================
  !       soil particle size parameters (cm) /midified for gcm16 and pla (0.05~54.83um)
  !
  real, parameter :: dmin = 0.00001 !< Minimum particules diameter \f$[cm]\f$
  real, parameter :: dmax = 0.011 !< Maximum particules diameter \f$[cm]\f$
  real, parameter :: dstep = 0.0365 !< Diameter increment \f$[cm]\f$
  !
  !       * reynolds constants and threshold value
  !
  real, parameter :: a_rnolds = 1331.647 !< Reynolds A constant
  real, parameter :: b_rnolds = 0.38194 !< Reynolds B constant
  real, parameter :: x_rnolds = 1.561228 !< Reynolds X constant
  real, parameter :: d_thrsld = 0.00000231 !< Reynolds threshold
  !
  !       * air and soil particle density (g/cm3)
  !
  real, parameter :: roa = 0.001227 !< Air density \f$[g/cm^3]\f$
  real, parameter :: rop = 2.65 !< Soil particle density \f$[g/cm^3]\f$
  !
  real, parameter :: umin = 55. !< Minimum threshold friction wind speed \f$[cm/sec]\f$
  !
  real, parameter :: vk = 0.4 !< von Karman constant
  !
  !       * efficient fraction
  !
  real, parameter :: aeff = 0.35 !< Efficient fraction
  real, parameter :: xeff = 10. !< Efficient fraction
  !
  real, parameter :: zz = 1000. !< Wind measureiment height \f$[cm]\f$
  !
  !       * surface roughness related parameters
  !
  real, parameter :: z0s = 0.001 !< Surface roughness related parameter
  real, parameter :: z01 = 0.001 !< Surface roughness related parameter
  real, parameter :: z02 = 0.001 !< Surface roughness related parameter
  !
  real, parameter :: pi = 3.141592653589793 !< Pi
  real, parameter :: gravi = 9.8*100. !< Gravitational acceleration in \f$[cm/s^2]\f$
  !
  real, parameter :: w0 = 0.25 !< Soil moisture threshold for CCCMABF=.TRUE.
  !        real, parameter :: w0 = 0.99        ! soil moisture threshold for cccmabf=.false.
  logical         :: cccmabf = .true. !< Use CCCma bare soil fraction or not
  !
  !       soil type data
  !
  !       solspe --> soil caracteristics:
  !       zobler texture classes
  !       solspe: for 4 populations : values = 3*(dmed sig p)
  !                                          ratio of fluxes
  !                                          residual moisture
  !
  !       populations: coarse sand, medium/fine sand, silt, clay
  !
  !       soil type 1 : coarse
  !       soil type 2 : medium
  !       soil type 3 : fine
  !       soil type 4 : coarse medium
  !       soil type 5 : coarse fine
  !       soil type 6 : medium fine
  !       soil type 7 : coarse_dp, medium_dp, fine
  !       soil type 8 : organic
  !       soil type 9 : ice
  !       soil type 10 : potential lakes (additional)
  !       soil type 11 : potential lakes (clay)
  !       soil type 12 : potential lakes australia
  !
  !
  real,  dimension(nspe,nats) :: solspe !<
  !
  data solspe / &
           0.0707, 2.0, 0.43, 0.0158, 2.0, 0.40, 0.0015, &
           2.0, 0.17, 0.0002, 2.0, 0.00, 2.1e-06, 0.20, &
           0.0707, 2.0, 0.00, 0.0158, 2.0, 0.37, 0.0015, &
           2.0, 0.33, 0.0002, 2.0, 0.30, 4.0e-06, 0.25, &
           0.0707, 2.0, 0.00, 0.0158, 2.0, 0.00, 0.0015, &
           2.0, 0.33, 0.0002, 2.0, 0.67, 1.0e-07, 0.50, &
           0.0707, 2.0, 0.10, 0.0158, 2.0, 0.50, 0.0015, &
           2.0, 0.20, 0.0002, 2.0, 0.20, 2.7e-06, 0.23, &
           0.0707, 2.0, 0.00, 0.0158, 2.0, 0.50, 0.0015, &
           2.0, 0.12, 0.0002, 2.0, 0.38, 2.8e-06, 0.25, &
           0.0707, 2.0, 0.00, 0.0158, 2.0, 0.27, 0.0015, &
           2.0, 0.25, 0.0002, 2.0, 0.48, 1.0e-07, 0.36, &
           0.0707, 2.0, 0.23, 0.0158, 2.0, 0.23, 0.0015, &
           2.0, 0.19, 0.0002, 2.0, 0.35, 2.5e-06, 0.25, &
           0.0707, 2.0, 0.25, 0.0158, 2.0, 0.25, 0.0015, &
           2.0, 0.25, 0.0002, 2.0, 0.25, 0.0e-00, 0.50, &
           0.0707, 2.0, 0.25, 0.0158, 2.0, 0.25, 0.0015, &
           2.0, 0.25, 0.0002, 2.0, 0.25, 0.0e-00, 0.50, &
           0.0707, 2.0, 0.00, 0.0158, 2.0, 0.00, 0.0015, &
           2.0, 1.00, 0.0002, 2.0, 0.00, 1.0e-05, 0.25, &
           0.0707, 2.0, 0.00, 0.0158, 2.0, 0.00, 0.0015, &
           2.0, 0.00, 0.0002, 2.0, 1.00, 1.0e-05, 0.25, &
           0.0707, 2.0, 0.00, 0.0158, 2.0, 0.00, 0.0027, &
           2.0, 1.00, 0.0002, 2.0, 0.00, 1.0e-05, 0.25, &
           0.0442, 1.5, 0.03, 0.0084, 1.5, 0.85, 0.0015, &
           2.0, 0.11, 0.0002, 2.0, 0.02, 1.9e-06, 0.12, &
           0.0450, 1.5, 0.00, 0.0070, 1.5, 0.33, 0.0015, &
           2.0, 0.50, 0.0002, 2.0, 0.17, 1.9e-04, 0.15, &
           0.0457, 1.8, 0.31, 0.0086, 1.5, 0.22, 0.0015, &
           2.0, 0.34, 0.0002, 2.0, 0.12, 3.9e-05, 0.13, &
           0.0293, 1.8, 0.39, 0.0090, 1.5, 0.16, 0.0015, &
           2.0, 0.35, 0.0002, 2.0, 0.10, 3.1e-05, 0.13, &
           0.0305, 1.5, 0.46, 0.0101, 1.5, 0.41, 0.0015, &
           2.0, 0.10, 0.0002, 2.0, 0.03, 2.8e-06, 0.12/
  !
end module duparm1
