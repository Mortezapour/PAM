PAM main page {#mainpage}
============

# PLA Aerosol Model (PAM) 

The piecewise log-normal approximation aerosol model (PAM) is an efficient and accurate method for the representation of 
particle size distributions in atmospheric models \cite vonSalzen2006.

1. @subpage PAMoverview
2. @subpage PAMtheory
3. @subpage PAMcode
    - @subpage codeIntro
    - @subpage PLArep
    - @subpage setupAndInit
        - @subpage dataTables
        - @subpage aerosolProperties 
        - @subpage GCMinit
        - @subpage errorsAndWarnings
    - @subpage codingConventions
        - @subpage dimAndAerosolTypeParams
        - @subpage arraysOnModelGrid
        - @subpage constantArrays
        - @subpage otherParams
        - @subpage subroutinesAndFunctions
    - @subpage callGraph
