# PAM Theory {#PAMtheory}

1. @ref ApproxDistr
2. @ref DeterFitParams
3. @ref Cost

# Approximation of the aerosol number distribution {#ApproxDistr}
\n\n 
 Without any loss of generality, an approximation of an aerosol number
 distribution may be expressed as a series of orthogonal functions, i.e.,
\n

\f{equation}{
 \label{eq:pla1}
 n (\varphi) = \sum_{i} n_{i} (\varphi)
\f}

\n
 for the dimensionless size parameter \f$\varphi \equiv \ln (
 R_{p}/R_{0})\f$.
\n 
 The following set of functions in Eq. (\f$\ref{eq:pla1}\f$) is proposed:
\n

\f{equation}{
 \label{eq:pla2}
 n_{i}(\varphi) = n_{0,i} \exp\left[{- \psi_{i} \left(\varphi - \varphi_{0,i}\right)^{2}}\right]
 H \left( \varphi - \varphi_{i-1/2} \right) H \left( \varphi_{i+1/2} - \varphi \right) 
\f}

\n
 where \f$n_{0,i}\f$, \f$\psi_{i}\f$, and \f$\varphi_{0,i}\f$ are fitting
 parameters that determine the magnitude, width, respectively mean of
 the distribution (section @ref DeterFitParams}). \f$H (x)\f$ denotes the
 Heaviside step function, i.e.
\n

\f{equation*}{ 
 H (x) = \begin{cases}
 0 & \text{if {x<0}}  \\
 \frac{1}{2} & \text{if {x=0}}  \\
 1 & \text{if {x>0} }
 \end{cases}
\f}

\n
 so that \f$n_{i}=0\f$ outside each section \f$i\f$ with the size range
 \f$\varphi_{i-1/2}\leq\varphi\leq\varphi_{i+1/2}\f$. Log-normal
 distributions with varying properties between sections are used as
 basis functions in each section [Eq.\f$\ref{eq:pla2}\f$]. Hence, Eqs.(\f$\ref{eq:pla1}\f$)
 and (\f$\ref{eq:pla2}\f$) define a piecewise log-normal
 approximation (PLA) of the aerosol number distribution.
\n 
 As will be demonstrated in the following sections,
 Eqs. (\f$\ref{eq:pla1}\f$) and (\f$\ref{eq:pla2}\f$) can be
 directly used for the approximation of observed size distributions and
 development of parameterizations in models.
\n 
 Note that similar to the single-moment bin approach (e.g., which can be
 obtained for \f$\psi_{i}=0\f$), the approach in Eq. (\f$\ref{eq:pla2}\f$)
 ensures that \f$n \left(\varphi \right) \ge 0\f$ for any given value of
 \f$\varphi\f$. This is not necessarily the case for other potential prescriptions,
 e.g. a representation of the basis functions in terms of polynomials.
 Additionally, the PLA method presented here has the advantages that
 the method is exact for strictly log-normally distributed aerosol
 number concentrations and that it is straightforward to calculate
 any higher moment of the approximated size distribution. The latter is
 particularly important for potential applications of the method
 to the development of parameterizations. Finally, approximated
 size distributions generally converge to the exact solution for a
 sufficiently large number of sections for appropriate choices of the
 fitting parameters.
\n\n


# Determination of the fitting parameters {#DeterFitParams}
\n 
 The fitting parameters \f$n_{0,i}\f$, \f$\psi_{i}\f$, and \f$\varphi_{0,i}\f$ in
 Eq. (\f$\ref{eq:pla2}\f$) can be obtained in various ways. Here it is proposed
 to prescribe one of these parameters and to calculate the other two
 parameters for given integrated number (\f$N_{i}\f$) and mass (\f$M_{i}\f$)
 concentrations in each section. This approach leads to a self-consistent
 representation of these quantities in numerical models and also
 leads to expressions that are relatively straightforward to evaluate.
\n
 First, it is useful to introduce the \f$k\f$th moment \f$\mu_{ki}\f$ of the
 size distribution in section \f$i\f$, i.e.

\f{equation*}{
 \mu_{ki} \equiv \int_{\varphi_{i-1/2}}^{\varphi_{i+1/2}} R_{p}^{k} n \left( \varphi
 \right) \, \varphi 
\f}

 Using Eqs. (\f$\ref{eq:pla1}\f$) and (\f$\ref{eq:pla2}\f$) it follows that

\f{equation*}{
 \mu_{ki} = R_{0}^{k} \, n_{0,i} \, I_{ki} 
\f}

 with

\f{equation*}{
 \label{eq:plaini}
 I_{ki} = \frac{1}{2}\sqrt{\frac{\pi}{|\psi_{i}|}} \,
 f_{ki} \, \exp\left({k \varphi_{0,i}+\frac{k^{2}}{4 |\psi_{i}|}}\right) 
\f}

 and

\f{equation*}{
 f_{ki} =
 \begin{cases}
 \text{erf} \left( a_{ki+1/2} \right) - \text{erf} \left( a_{ki-1/2} \right)
 & \text{if } psi_{i} > 0  \\
 \text{erfi} \left( a_{ki+1/2} \right) - \text{erfi} \left( a_{ki-1/2} \right)
 & \text{if } psi_{i} < 0 
 \end{cases}
\f}

 The arguments of the error function \f$\text{erf} (x)\f$
 and imaginary error function \f$\text{erfi} (x)\f$ in \f$f_{ki}\f$ are given by:

\f{equation*}{
 a_{ki+1/2}  = - \sqrt{|\psi_{i}|} \, \left( \Delta \varphi_{i} - \Delta
 \varphi_{\star} \right) - \frac{k sgn \left( \psi_{i}
 \right)}{2 \sqrt{|\psi_{i}|}}  \\
\f}

\f{equation*}{
 a_{ki-1/2}  = - \sqrt{|\psi_{i}|} \, \Delta
 \varphi_{i} - \frac{k sgn \left( \psi_{i}
 \right)}{2 \sqrt{|\psi_{i}|}} 
\f}

 with \f$ sgn (x) = 2 H(x) - 1\f$,
\n

\f{equation}{
 \label{eq:dphi}
 \Delta \varphi_{i}\equiv\varphi_{0,i} - \varphi_{i-1/2} 
\f}

\n 
and section size \f$\Delta \varphi_{\star} \equiv \varphi_{i+1/2} - \varphi_{i-1/2}\f$.
\n 
 Consequently, the integrated number (\f$k\f$=0) and mass (\f$k\f$=3) concentrations in each
 section are:

\f{equation}{
 \label{eq:plani}
 N_{i}  = n_{0,i} \, I_{0i} 
\f}

\f{equation}{
 \label{eq:plami}
 M_{i}  = \frac{4 \pi}{3} \rho_{p,i} R^{3}_{0} \, n_{0,i} \, I_{3i} 
\f}

 where \f$\rho_{p,i}\f$ is the particle density,
\n
 As shall be seen, it is useful for the calculation of the fitting
 parameters to also introduce the dimensionless size ratio
\n

\f{equation}{
 \label{eq:ri}
 r_{i} \equiv
 \frac{\hat{\varphi}_{i}-\varphi_{i-1/2}}{\Delta \varphi_{\star}} 
\f}

 where

\f{equation*}{
 \hat{\varphi}_{i} \equiv \ln \left[ \frac{1}{R_{0}}
 \left( \frac{3}{4 \pi \rho_{p,i}} \frac{M_{i}}{N_{i}} \right)^{1/3}
 \right] 
\f}

 is an effective particle size.
\n 
\f$r_{i}\f$ [Eq.(\f$\ref{eq:ri}\f$)] characterizes the skewness of the
 approximated size distribution
 within each section. For example, consider two populations of
 aerosol particles that have the same total number of particles
 but different total mass concentrations within a given range of
 particle sizes. This can only be the case if the population with the
 higher mass concentration includes fewer of the small and more of the big
 particles compared to the other population. Hence, the number
 size distribution of the former aerosol population is skewed towards larger
 particle sizes relative to the other number size distribution. Any
 meaningful values of \f$N_{i}\f$ and \f$M_{i}\f$ require that \f$0 \leq r_{i} \leq
 1\f$. Mathematically, the boundary of the manifold that contains
 the solutions to Eqs. (\f$\ref{eq:plani}\f$) and (\f$\ref{eq:plami}\f$) is
 given by delta-distributed particle number concentrations for minimum
 and maximum particle sizes in the given size range (i.e. corresponding
 to the section boundaries).
\n 
 Combination of an expression for \f$M_{i}/N_{i}\f$ from
 Eqs. (\f$\ref{eq:plani}\f$) and (\f$\ref{eq:plami}\f$) with
 Eq. (\f$\ref{eq:ri}\f$) conveniently eliminates \f$n_{0,i}\f$, i.e.
\n

\f{equation}{
 \label{eq:plamain}
 \frac{f_{3i}}{f_{0i}} =
 \exp\left[{3 \left(r_{i} \Delta \varphi_{\star} - \Delta \varphi_{i} \right)
 - \frac{9}{4 |\psi_{i}|} }\right]
\f}

\n
 The only unknown parameters in this equation are \f$\Delta \varphi_{i}\f$
 and \f$\psi_{i}\f$ if \f$N_{i}\f$ and \f$M_{i}\f$ and the section boundaries
 are given. It is suggested to prescribe \f$\psi_{i}\f$ so that this
 equation can be used to determine \f$\Delta \varphi_{i}\f$. Subsequently,
 Eq. (\f$\ref{eq:dphi}\f$) can be used to determine the value of the fitting
 parameter \f$\varphi_{0,i}\f$.
\n
 An example for \f$\Delta \varphi_{i}\f$ from the solution of
 Eq. (\f$\ref{eq:plamain}\f$) is shown in Fig.\ref I for a section size
 \f$\Delta \varphi_{\star} = \frac{1}{3}[\ln (1.75) - \ln (0.05)]\f$

\anchor I
\image html Fig1.png "Fig.I ∆φi [Eq. (3)] according to Eq. (7). No solution exists in the shaded area. " width=500px

 Note that Eq. (\f$\ref{eq:plamain}\f$) does not have any solution for
 \f$\Delta \varphi_{i}\f$ within a certain range of values of \f$\psi_{i}\f$
 and \f$r_{i}\f$. Therefore, \f$\psi_{i}\f$ is selected such that \f$\psi_{i}\f$
 is equal to an arbitrarily prescribed value \f$\psi_{m,i}\f$ inside the region for
 which a solution of Eq. (\f$\ref{eq:plamain}\f$) exists. Outside that
 region, a different value than that is selected in order to
 ensure that a solution exists. In that case, \f$\psi_{i}\f$ should be as
 close as possible to \f$\psi_{m,i}\f$. Therefore,
\n

\f{equation}{
 \label{eq:plait}
 \psi_{i} = \begin{cases}
 \min \left( \psi_{m,i}, \psi_{l} \right) & \text{if } \psi_{m,i} < 0  \\
 \max \left( \psi_{m,i}, \psi_{l} \right) & \text{if } \psi_{m,i} > 0 
 \end{cases}
\f}

\n
 where \f$\psi_{l}\f$ represents a threshold for which a solution of
 Eq. (\f$\ref{eq:plamain}\f$) can be found (i.e. as indicated by the green
 line in Fig.\ref I This algorithm can be applied to
 \f$0 \leq r_{i} \leq 1\f$ and \f$-\infty \leq \psi_{m,i} \leq \infty\f$ for

\f{equation*}{
 \lim_{r_{i} \to 0, 1} \psi_{l} =
 \begin{cases}
 +\infty & \text{if } \psi_{i} > 0  \\
 -\infty & \text{if } \psi_{i} < 0 
 \end{cases}
\f}

\n 
 In practice, it should be sufficient to create a look-up table for
 \f$\Delta \varphi_{i}\f$ as a function of \f$\psi_{i}\f$ and \f$r_{i}\f$ and to
 use this result to obtain the complete set of fitting parameters
 from Eqs. (\f$\ref{eq:dphi}\f$) and (\f$\ref{eq:plani}\f$) or (\f$\ref{eq:plami}\f$).
 Although not necessary, it is convenient to assume that all sections have the
 same size so that only one look-up table is required.
\n 
 The algorithm described above is summarized in Fig.\ref II  It
 provides the fitting parameters for each section in
 Eq. (\f$\ref{eq:pla2}\f$) for given values of the number and mass
 concentrations in the same section.
 
\anchor II
\image html Fig2.png "Fig.II  Summary of the algorithm to determine the fitting parameters in Summary of the algorithm to determine the fitting parameters in Eq.(2). In applications of the algorithm in models, the steps in the box at the top should be part of the pre-processing stage in the model. The inputs for the other steps may change at each time step in the model and will generally have to be repeated once size distributions are updated in the model." width=400px

\n 
 While the accuracy of the approximation of a size distribution in
 terms of log-normal size distributions according to
 Eq. (\f$\ref{eq:pla2}\f$) depends on the values of \f$\psi_{m,i}\f$, no attempt
 is made here to calculate \f$\psi_{m,i}\f$ as part of the algorithm.
 Tests with the method under various conditions give evidence for
 a relatively weak dependency of the errors of the method
 on \f$\psi_{m,i}\f$ within a substantial range for this parameter.
\n 
 The accuracy of the transformations from \f$N_{i}\f$ and \f$M_{i}\f$ to
 log-normal size distributions according to the algorithm
 (Fig.\ref II ) and vice versa [Eqs. (\f$\ref{eq:plani}\f$) and
 (\f$\ref{eq:plami}\f$)] depends on the accuracy of the tabulated
 data for \f$\Delta \varphi_{i}\f$. However, for typical applications,
 (e.g. in the examples presented in the following sections) sufficient
 accuracy can usually be achieved for reasonably small table sizes. If
 necessary, the accuracy can be additionally increased by directly
 solving Eq. (\f$\ref{eq:plamain}\f$) instead of (or in addition to) the table
 look-up step.
\n 
 It should be noted here that similar to the approach proposed here, \cite Zender2003
 proposed a piecewise-analytical representation of the aerosol size
 distribution. Specifically, they assumed that the aerosol mass is log-normally
 distributed in each section. However, their approach is quite similar to the
 single-moment sectional approach. In particular, the assumption of time-invariant
 size distributions within each section, with the mass being the only predicted
 variable, clearly distinguishes their approach from the PLA method. Consequently,
 aerosol number (or any other moment) will generally not be conserved in simulations
 with Zender et al.'s approach. Additionally, the assumption of a time-invariant size
 distribution could be quite problematic at small to moderate numbers
 of sections. For example, this assumption cannot be expected to not
 work well for gravitational settling of mineral dust particles which
 typically causes large variations in the particle size
 distribution with height. In contrast to Zender et al.'s approach, the PLA method
 can be used to account for changes in the size distribution at any
 given particle size scale.
\n\n

# Computational costs of the method {#Cost}
\n 
 For applications of the PLA method in numerical models it is useful to
 consider the computational costs that are potentially associated with
 the method. In a model, the calculations of \f$r_{i}\f$ and \f$n_{0,i}\f$
 in Fig.\ref II would likely be the most expensive steps
 because of the need to evaluate the functions \f$\ln (x)\f$ and \f$\text{erf} (x)\f$
 [respectively \f$\text{erfi} (x)\f$] for each section according to Eqs. (\f$\ref{eq:ri}\f$) and
 (\f$\ref{eq:plani}\f$). In comparison, the costs associated with the table look-up are
 relatively minor for a typical Fortran 90-implementation on an IBM pSeries 390
 system with POWER4 microprocessor architecture at the Canadian
 Meteorological Centre (i.e. \f$\approx 1/3\f$ of the total costs of the
 method). These costs are considerably smaller than the costs that would
 typically be associated with parameterizations of bulk aerosol processes in
 atmospheric models.
\n

---

This material is adapted from "von Salzen, K.: Piecewise log-normal approximation of size distributions for aerosol modelling, Atmos. Chem. Phys., 6, 1351–1372, https://doi.org/10.5194/acp-6-1351-2006, 2006." \cite vonSalzen2006
