subroutine ssaltaerop(exta, exoma, exomga, fa, absa, exta055, &
                            exta086, ssa055, rhin, aload, re, ve, &
                            il1, il2, ilg, lay)
  !
  !     * feb 07/2015 - k.vonsalzen. revised version for gcm18:
  !     *                            - expanded sizes for rhnode,renode,
  !     *                              venode,fr2node.
  !     *                            - changes to max/min bounds for
  !     *                              ve and rh.
  !     * 2012.4 - J.LI.   PLA VERSION BASED ON KNUT'S GROWTH METHOD,
  !                        no crystalazation point.
  !     * 2006.9 - li & ma continuous size scheme
  !----------------------------------------------------------------------c
  !     calculation of optical properties for sea salt aerosol           c
  !     based on wet ssalt concentration, with log norm distribution     c
  !                                                                      c
  !     exta:    extinction coefficient * aload                          c
  !     exoma:   exta times single scattering albedo                     c
  !     exomga:  exoma times asymmetry factor                            c
  !     fa:      square of asymmetry factor                              c
  !     absa:    absorption coefficient * albedo                         c
  !                                                                      c
  !     exta055: extinction coefficient at 0.55 um * aload               c
  !     exta086: extinction coefficient at 0.865 um * aload              c
  !     ssa055:  single scattering albedo at 0.55 um                     c
  !     ga055:   asymmetry factor at 0.55 um                             c
  !                                                                      c
  !     rhin:    input relative humidity                                 c
  !     aload:   sea salt aerosol mass ratio for each layer (kg/kg)      c
  !     re:      effective radius                                        c
  !     rh:      relative humidity                                       c
  !                                                                      c
  !     sext:    first 4 are results for 4 solar bands, the fifth is     c
  !              for 0.55 um, the last is for 0.865 um                   c
  !     somg/sg: for 4 are for 4 solar bands, the last for 0.55 um       c
  !     sabs:    first 9 are results for 9 lw bands                      c
  !----------------------------------------------------------------------c
  !
  use rdmod
  !
  implicit real (a-h,o-z),  integer (i-n)
  !
  real, dimension(ilg,lay,nbs) :: exta !<
  real, dimension(ilg,lay,nbs) :: exoma !<
  real, dimension(ilg,lay,nbs) :: exomga !<
  real, dimension(ilg,lay,nbs) :: fa !<
  real, dimension(ilg,lay,nbl) :: absa !<
  real, dimension(nbs) :: sexta !<
  real, dimension(nbs) :: somga !<
  real, dimension(nbs) :: sga !<
  real                   :: sext(6) !<
  real                   :: somg(5) !<
  real                   :: sg(5) !<
  real                   :: sabs(9) !<
  real, dimension(nbl) :: sabsa !<
  real, dimension(ilg,lay) :: exta055 !<
  real, dimension(ilg,lay) :: exta086 !<
  real, dimension(ilg,lay) :: ssa055 !<
  real, dimension(ilg,lay) :: ga055 !<
  real, dimension(ilg,lay) :: rh !<
  real, dimension(ilg,lay) :: re !<
  real, dimension(ilg,lay) :: ve !<
  real, dimension(ilg,lay) :: fr1 !<
  real, dimension(ilg,lay) :: fr2 !<
  real, dimension(ilg,lay) :: aload !<
  real, dimension(ilg,lay) :: rhin !<
  real, dimension(ilg,lay) :: sload !<
  real  :: rhnode(nhs) !<
  real  :: renode(nrs) !<
  real  :: venode(nvs) !<
  real, dimension(2) :: th !<
  real, dimension(2) :: tr !<
  real, dimension(2) :: tv !<
  !
  data rhnode  /0.1, 0.5, 0.75, 0.85, 0.9, 0.93, 0.95, 0.96, 0.97, &
               0.98, 0.99/
  data renode  /0.7, 1.7, 2.7, 3.7, 4.7, 5.7/
  data venode  /0.4, 0.6, 0.8/
  !
  !----------------------------------------------------------------------c
  !     factor 10000, because the unit of specific extinction for aerosolc
  !     is m^2/gram, while for gas is cm^2/gram, in raddriv the same dp  c
  !     (air density * layer thickness) is used for both gas and aerosol.c
  !     aload is dry loading in unit g (aerosol) / g(air).               c
  !----------------------------------------------------------------------c
  !
  do k = 1, lay
    do i = il1, il2
      if (aload(i,k) > 1.e-12) then
        !
        sload(i,k)      =  10000. * aload(i,k)
        rh(i,k)         =  max(min (rhin(i,k), 0.95), 0.1)
        re(i,k)         =  max(min (re(i,k), 5.7), 0.7)
        ve(i,k)         =  max(min (ve(i,k), 0.8), 0.4)
        !
        ih              =  mvidx(rhnode, nhs, rh(i,k))
        ir              =  mvidx(renode, nrs, re(i,k))
        iv              =  mvidx(venode, nvs, ve(i,k))
        !
        th(2)           = (rh(i,k) - rhnode(ih)) / &
                          (rhnode(ih+1) - rhnode(ih))
        th(1)           =  1.0 - th(2)
        tr(2)           = (re(i,k) - renode(ir)) / &
                          (renode(ir+1) - renode(ir))
        tr(1)           =  1.0 - tr(2)
        tv(2)           = (ve(i,k) - venode(iv)) / &
                          (venode(iv+1) - venode(iv))
        tv(1)           =  1.0 - tv(2)
        !
        do j = 1, nbs
          sexta(j)      =  0.0
          somga(j)      =  0.0
          sga(j)        =  0.0
        end do
        sexta055        =  0.0
        sexta086        =  0.0
        somga055        =  0.0
        sga055          =  0.0
        !
        do j = 1, nbl
          sabsa(j)      =  0.0
        end do
        !
        do ihh      =  ih, ih + 1
          do irr      =  ir, ir + 1
            do ivv      =  iv, iv + 1
              !
              irec          = (ihh - 1) * nrs * nvs + &
                          (irr - 1) * nvs + iv
              !
              sext=sextt2(irec,:)
              somg=somgt2(irec,:)
              sg=sgt2(irec,:)
              sabs=sabst2(irec,:)
              !
              wtt           =  th(ihh - ih + 1) * tr(irr - ir + 1) * &
                          tv(ivv - iv + 1)
              !
              do j = 1, nbs
                sexta(j)    =  sexta(j) + sext(j) * wtt
                somga(j)    =  somga(j) + somg(j) * wtt
                sga(j)      =  sga(j) + sg(j) * wtt
              end do
              sexta055    =  sexta055 + sext(5) * wtt
              sexta086    =  sexta086 + sext(6) * wtt
              somga055    =  somga055 + somg(5) * wtt
              sga055      =  sga055   + sg(5) * wtt
              !
              do j = 1, nbl
                sabsa(j)    =  sabsa(j) + sabs(j) * wtt
              end do
              !
            end do
          end do
        end do ! loop 100
        !
        !----------------------------------------------------------------------c
        !     the results of exta, exoma, exomga, fa, absa are accumulated     c
        !     with dust & ssalt calculated in other subroutines                c
        !----------------------------------------------------------------------c
        !
        do j = 1, nbs
          extload       =  sexta(j) * sload(i,k)
          exta(i,k,j)   =  exta(i,k,j) + extload
          extomload     =  extload * somga(j)
          exoma(i,k,j)  =  exoma(i,k,j) + extomload
          extomgload    =  extomload * sga(j)
          exomga(i,k,j) =  exomga(i,k,j) + extomgload
          fa(i,k,j)     =  fa(i,k,j) + extomgload * sga(j)
        end do
        exta055(i,k)  =  sexta055
        ssa055(i,k)   =  somga055
        ga055(i,k)    =  sga055
        exta086(i,k)  =  sexta086
        !
        do j = 1, nbl
          absa(i,k,j)   =  absa(i,k,j) + sabsa(j) * sload(i,k)
        end do
        !
      else
        exta055(i,k)  =  0.0
        ssa055(i,k)   =  0.0
        ga055(i,k)    =  0.0
        exta086(i,k)  =  0.0
      end if
    end do
  end do ! loop 200
  !
  return
end
