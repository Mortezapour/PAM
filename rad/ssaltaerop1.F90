!> \file ssaltaerop.F90
!>\brief Calculate sea salt aerosol optical properties for PAM aerosol code
!!
!! @author Jiangnan Li
!
subroutine ssaltaerop1(exta, exoma, exomga, fa, absa, exta055, &
                       exta086, ssa055, rhin, aload, re, ve, &
                       il1, il2, ilg, lay)
  !
  !     * Nov    2021 - j. li        - change the input to dry sea salt and 
  !                                    change the size range  
  !     * feb 07/2015 - k.vonsalzen. revised version for gcm18:
  !     *                            - expanded sizes for rhnode,renode,
  !     *                              venode,fr2node.
  !     *                            - changes to max/min bounds for
  !     *                              ve and rh.
  !     * 2012.4 - j.li.   pla version based on knut's growth method,
  !                        no crystalazation point.
  !     * 2006.9 - li & ma continuous size scheme
  !
  use ssaltdata, only : ssaltsw, ssaltlw 
  implicit none
  integer, intent(in) :: il1  !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2  !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: ilg  !< Total number of atmospheric columns \f$[unitless]\f$
  integer, intent(in) :: lay  !< Number of vertical layers \f$[unitless]\f$
  integer, parameter  :: nbs = 4
  integer, parameter  :: nbl = 9
  integer, parameter  :: nh = 11
  integer, parameter  :: nr = 6
  integer, parameter  :: nv = 3

!  
  real, intent(inout) :: exta(ilg,lay,nbs)  !< Extinction coefficient \f$[m^2/gram]\f$
  real, intent(inout) :: exoma(ilg,lay,nbs) !< EXTA*single scattering albedo \f$[m^2/gram]\f$
  real, intent(inout) :: exomga(ilg,lay,nbs)!< EXOMA*asymetry factor \f$[m^2/gram]\f$
  real, intent(inout) :: fa(ilg,lay,nbs)    !< EXOMGA*asymetry factor \f$[m^2/gram]\f$
  real, intent(inout) :: absa(ilg,lay,nbl)  !< Longwave absorption coefficient \f$[m^2/gram]\f$
  real, intent(inout) :: exta055(ilg,lay)   !< Extinction coefficient at 0.55 \f$[m^2/gram]\f$
  real, intent(inout) :: ssa055(ilg,lay)    !< Single scattering albedo at 0.55 \f$[unitless]\f$
  real, intent(inout) :: exta086(ilg,lay)   !< Extinction coefficient at 0.86 \f$[m^2/gram]\f$
  real, intent(in)    :: aload(ilg,lay)     !< Aerosol loading of dust \f$[gram/gram]\f$
  real, intent(inout) :: re(ilg,lay)        !< Effective radius \f$[micro meter]\f$
  real, intent(inout) :: ve(ilg,lay)        !< Effective variance of dust \f$[unitless]\f$
  real, intent(in)    :: rhin(ilg,lay)      !< Relative humidity \f$[\%]\f$
  !
  integer, external   :: mvidx
  !
  integer :: i
  integer :: j
  integer :: k
  integer :: ih
  integer :: ihh
  integer :: ir
  integer :: irr
  integer :: iv
  integer :: ivv
  integer :: irec
  real :: extload
  real :: extomgload
  real :: extomload
  real :: sexta055
  real :: sexta086
  real :: sga055
  real :: somga055
  real :: wtt
  !
  real, dimension(2) :: th
  real, dimension(2) :: tr
  real, dimension(2) :: tv
  real, dimension(nbs) :: sexta
  real, dimension(nbs) :: somga
  real, dimension(nbs) :: sga
  real, dimension(nbl) :: labs
  real, dimension(17):: ss
  real, dimension(6) :: sext
  real, dimension(5) :: somg
  real, dimension(5) :: sg
  real, dimension(9) :: lsab
  real, dimension(nbl) :: lsaba
  real, dimension(ilg,lay) :: ga055 
  real, dimension(ilg,lay) :: rh 
  real, dimension(ilg,lay) :: sload 
  !
  !     calculation of optical properties for sea salt aerosol
  !     based on wet ssalt concentration, with log norm distribution
  !     The interoplation is based on three dimensional spheriod
  !     of relative humidity, effective radius and variance.
  !
  !     ssaltsw: 1 the growth factor, 2-5 ext for 4
  !              solar bands, 6-7 for 0.55 and 0.86 um, 8-11 single scatt
  !              albedo for 4 bands, 12 for 0.55 um, 13-16 g for 4 bands
  !              17 g for 0.55 um
  !     ssaltlw: 9 data are results of absorption coeffocient for 9 lw bands
  !
  real, parameter, dimension(nh) :: rhnode = [0.1, 0.5, 0.75, 0.85, 0.9, 0.93, &
                                              0.95, 0.96, 0.97, 0.98, 0.99]
  real, parameter, dimension(nr) :: renode = [0.1, 0.7, 1.3, 1.9, 2.5, 3.1]
  real, parameter, dimension(nv) :: venode = [0.1, 0.5, 2.0]
  !
  !     factor 10000, because the unit of specific extinction for aerosol
  !     is m^2/gram, while for gas is cm^2/gram, in raddriv the same dp  
  !     (air density * layer thickness) is used for both gas and aerosol.
  !     aload is dry loading in unit g (aerosol) / g(air), timing of the
  !     ratio of wet mass / dry mass becomes, wet loading    
  !
  do k = 1, lay
    do i = il1, il2
      if (aload(i,k) > 1.e-12) then
        !
        sload(i,k)       =  10000. * aload(i,k)
        rh(i,k)          =  max(min (rhin(i,k), 0.95), 0.1)
        re(i,k)          =  max(min (re(i,k), 3.1), 0.1)
        ve(i,k)          =  max(min (ve(i,k), 2.0), 0.1)
        !
        ih               =  mvidx(rhnode, nh, rh(i,k))
        ir               =  mvidx(renode, nr, re(i,k))
        iv               =  mvidx(venode, nv, ve(i,k))
        !
        th(2)            = (rh(i,k) - rhnode(ih)) / (rhnode(ih + 1) - rhnode(ih))
        th(1)            =  1.0 - th(2)
        tr(2)            = (re(i,k) - renode(ir)) / (renode(ir + 1) - renode(ir))
        tr(1)            =  1.0 - tr(2)
        tv(2)            = (ve(i,k) - venode(iv)) / (venode(iv + 1) - venode(iv))
        tv(1)            =  1.0 - tv(2)
        !
        do j = 1, nbs
          sexta(j)       =  0.0
          somga(j)       =  0.0
          sga(j)         =  0.0
        end do
        sexta055         =  0.0
        sexta086         =  0.0
        somga055         =  0.0
        sga055           =  0.0
        !
        do j = 1, nbl
          lsaba(j)       =  0.0
        end do
        !
        do ihh      =  ih, ih + 1
          do irr      =  ir, ir + 1
            do ivv      =  iv, iv + 1
              !
              irec       = (ihh - 1) * nr * nv + (irr - 1) * nv + iv
              !
              ss(1:17)   =  ssaltsw(1:17,irec)
              sext(1:6)  =  ss(2:7)
              somg(1:5)  =  ss(8:12)
              sg(1:5)    =  ss(13:17)
              labs(1:9)  =  ssaltlw(1:9,irec)
              !
              wtt        =  th(ihh - ih + 1) * tr(irr - ir + 1) * &
                            tv(ivv - iv + 1)
              !
              do j = 1, nbs
                sexta(j) =  sexta(j) + sext(j) * ss(1) * wtt
                somga(j) =  somga(j) + somg(j) * wtt
                sga(j)   =  sga(j) + sg(j) * wtt
              end do
              sexta055   =  sexta055 + sext(5) * ss(1) * wtt
              sexta086   =  sexta086 + sext(6) * ss(1) * wtt
              somga055   =  somga055 + somg(5) * wtt
              sga055     =  sga055   + sg(5) * wtt
              !
              do j = 1, nbl
                lsaba(j) =  lsaba(j) + lsab(j) * ss(1) * wtt
              end do
              !
            end do
          end do
        end do 
        !
        !     the results of exta, exoma, exomga, fa, absa are accumulated    
        !     with dust & ssalt calculated in other subroutines              
        !
        do j = 1, nbs
          extload        =  sexta(j) * sload(i,k)
          exta(i,k,j)    =  exta(i,k,j) + extload
          extomload      =  extload * somga(j)
          exoma(i,k,j)   =  exoma(i,k,j) + extomload
          extomgload     =  extomload * sga(j)
          exomga(i,k,j)  =  exomga(i,k,j) + extomgload
          fa(i,k,j)      =  fa(i,k,j) + extomgload * sga(j)
        end do
        exta055(i,k)     =  sexta055
        ssa055(i,k)      =  somga055
        ga055(i,k)       =  sga055
        exta086(i,k)     =  sexta086
        !
        do j = 1, nbl
          absa(i,k,j)    =  absa(i,k,j) + lsaba(j) * sload(i,k)
        end do
        !
      else
        exta055(i,k)     =  0.0
        ssa055(i,k)      =  0.0
        ga055(i,k)       =  0.0
        exta086(i,k)     =  0.0
      end if
    end do
  end do 
  !
  return
end subroutine ssaltaerop1
!> \file
!> Calculation of optical properties for sea salt in continous mode (PAM)
!! A library of optical properties are computed for 6 effective radii,
!! which are linerly interpolated to any effective radius between them.
!! For effective variance the 3-point Lagrangian interoplation
!! method is used.
