      SUBROUTINE XTEVOLC4(XEMIS,PRESSG,DSHJ,ZF,                 &
                          ESCVROW,EHCVROW,ESEVROW,EHEVROW,      &
                          ISO2,                                 &
                          ILG,IL1,IL2,ILEV,NTRAC,ZTMST)
!
!     * JUN 18/2013 - K.VONSALZEN/ NEW VERSION FOR GCM17+:
!     *               M.LAZARE.    - ADD CONDITION TO SET WGT=1. FOR
!     *                              SPECIAL CASE WHERE HSO2=LSO2.
!     *                            - ADD SUPPORT FOR THE PLA OPTION
!     *                              "DOC" ARRAY, "IPAM" SWITCH NOW PASSED.
!     *                            - REMOVE "EHCVROW" AND "EHEVROW"
!     *                              FROM IF CONDITIONS.
!     * APR 28/2012 - K.VONSALZEN/ PREVIOUS VERSION XTEVOLC3 FOR GCM16:
!     *               M.LAZARE.    - INITIALIZE LSO2 AND HSO2 TO
!     *                              ILEV INSTEAD OF ZERO, TO
!     *                              AVOID POSSIBLE INVALID
!     *                              MEMORY REFERENCES.
!     *                            - BUGFIX TO INCLUDE RATIO OF
!     *                              MOLECULAR WEIGHTS IN
!     *                              CALCULATION OF CONTINUOUS
!     *                              VOLCANIC EMISSIONS.
!     * APR 26/2010 - K.VONSALZEN/ PREVIOUS VERSION XTEVOLC2 FOR GCM15I:
!     *               M.LAZARE.    - ZF PASSED IN (CALCULATED IN
!     *                              PHYSICS) RATHER THAN CALCULATED
!     *                              INTERNALLY (NO NEED TO PASS IN
!     *                              T OR SHBJ).
!     *                            - EXPLOSIVE VOLCANOES NOT CONTROLLED
!     *                              BY PASSED-IN "IEXPLVOL" SWITCH;
!     *                              RATHER A NEW LOGICAL INTERNAL
!     *                              VARIABLE "VOLCEXP" IS USED
!     *                              DEFAULT "OFF") WHICH CAN BE
!     *                              UPDATED TO BE ACTIVATED. WE
!     *                              DO EXPLOSIVE VOLCANOES EXTERNALLY
!     *                              TO THIS ROUTINE GENERALLY NOW.
!     *                            - EMISSIONS NOW STORED INTO GENERAL
!     *                              "EMIS" ARRAY, INSTEAD OF UPDATING
!     *                              XROW AT THIS POINT.
!     *                            - BUGFIX TO VERTICAL LOOP 205 WHERE
!     *                              SHOULD START FROM "ILEV" AND NOT
!     *                              "LEV" (OTHERWISE MEMORY OUT OF
!     *                              BOUNDS).
!     *                            - MORE ACCURATE CALCULATION OF "WGT".
!     * FEB 07,2009 - K.VONSALZEN/ PREVIOUS ROUTINE XTEVOLC FOR GCM15H:
!     *               M.LAZARE:    - APPLY EMISSIONS FROM VOLCANOES.
!     *                              THIS USED TO BE IN OLD XTEAERO.
!     *               **NOTE**:      WE PASS IN AND USE INTEGER SWITCH
!     *                              "IEXPLVOL" TO CONTROL WHETHER
!     *                              EXPLOSIVE VOLCANO CODE IN THIS
!     *                              ROUTINE IS ACTIVATED (IEXPLVOL=0)
!     *                              OR IS HANDLED ELSEWHERE IN THE
!     *                              CODE (IEXPLVOL=1).
!
      IMPLICIT NONE
!
!     * I/O FIELDS.
!
      INTEGER, INTENT(IN) :: ILG,ILEV,NTRAC,ISO2,IL1,IL2
      REAL, INTENT(IN)    :: ZTMST
      REAL, INTENT(IN)    :: DSHJ(ILG,ILEV),PRESSG(ILG)
      REAL, INTENT(IN)    :: ZF(ILG,ILEV)
!
      REAL, INTENT(IN)    :: ESCVROW(ILG),EHCVROW(ILG)
      REAL, INTENT(IN)    :: ESEVROW(ILG),EHEVROW(ILG)
!
      REAL, INTENT(INOUT) :: XEMIS(ILG,ILEV,NTRAC)
!
!     * INTERNAL WORK FIELDS.
!
      INTEGER, DIMENSION(ILG) ::  LSO2,HSO2
!
      REAL, PARAMETER :: WSO2    = 64.059
      REAL, PARAMETER :: WH2SO4  = 98.073
      REAL, PARAMETER :: WNH3    = 17.030
      REAL, PARAMETER :: WAMSUL  = WH2SO4+2.*WNH3
      REAL, PARAMETER :: WRAT    = WAMSUL/WSO2
      REAL, PARAMETER :: GRAV    = 9.80616
!
!       COMMON /PARAMS/ WW,     TW,    A,     ASQ,  GRAV, RGAS,  RGOCP,  RGOASQ, CPRES, RGASV, CPRESV
!
!     *         LOCAL FLAG TO CONTROL EXPLOSIVE CLIMATOLOGICAL
!     *         (NON-TRANSIENT) VOLCANOES.
!
    LOGICAL VOLCEXP
    DATA VOLCEXP /.FALSE./

    INTEGER :: IL,L,JT
    REAL :: ELCV,EUCV,FACT,WGT,ELEV,EUEV
!-------------------------------------------------------------------
!
!     * ESCV: CONTINOUS VOLCANIC EMISSIONS
!
    DO IL=IL1,IL2
        LSO2(IL) = ILEV
        HSO2(IL) = ILEV
    ENDDO
!
    DO L=ILEV,1,-1
        DO IL=IL1,IL2
            IF(ZF(IL,L) .LE. 0.67*EHCVROW(IL))   LSO2(IL)=L
            IF(ZF(IL,L) .LE. 1.00*EHCVROW(IL))   HSO2(IL)=L
        ENDDO
    ENDDO
!
    JT=ISO2
    DO L=1,ILEV
      DO IL=IL1,IL2
        IF(ESCVROW(IL).GT.0.) THEN
          ELCV=0.67*EHCVROW(IL)
          EUCV=1.00*EHCVROW(IL)
          FACT = ZTMST*GRAV/(DSHJ(IL,L)*PRESSG(IL))
          IF(L.EQ.HSO2(IL) .AND. L.EQ.LSO2(IL)) THEN
            WGT=1.
          ELSE IF(L.EQ.HSO2(IL)) THEN
            WGT=(EUCV-ZF(IL,L))/(EUCV-ELCV)
          ELSE IF(L.GT.HSO2(IL).AND.L.LT.LSO2(IL)) THEN
            WGT=(ZF(IL,L-1)-ZF(IL,L))/(EUCV-ELCV)
          ELSE IF(L.EQ.LSO2(IL)) THEN
            WGT=(ZF(IL,L-1)-ELCV)/(EUCV-ELCV)
          ELSE
            WGT=0.
          ENDIF
          XEMIS(IL,L,JT)=XEMIS(IL,L,JT)+WGT*ESCVROW(IL)*FACT*32.064/64.059
        ENDIF
      ENDDO
    ENDDO
!
!     * ESEV: ERUPTIVE VOLCANIC EMISSIONS
!     * NOTE! THESE ARE ONLY DONE IF THE LOCAL SWITCH "VOLCEXP" IS .TRUE.
!
      IF (VOLCEXP) THEN
        DO IL=IL1,IL2
          LSO2(IL) = ILEV
          HSO2(IL) = ILEV
        ENDDO
!
        DO L=ILEV,1,-1
          DO IL=IL1,IL2
            IF(ZF(IL,L) .LE. EHEVROW(IL)+500. )   LSO2(IL)=L
            IF(ZF(IL,L) .LE. EHEVROW(IL)+1500.)   HSO2(IL)=L
          ENDDO
        ENDDO
!
        DO L=1,ILEV
            DO IL=IL1,IL2
              IF(ESEVROW(IL).GT.0.) THEN
                ELEV=EHEVROW(IL)+500.
                EUEV=EHEVROW(IL)+1500.
                FACT = ZTMST*GRAV/(DSHJ(IL,L)*PRESSG(IL))
                IF(L.EQ.HSO2(IL) .AND. L.EQ.LSO2(IL)) THEN
                  WGT=1.
                ELSE IF(L.EQ.HSO2(IL)) THEN
                  WGT=(EUEV-ZF(IL,L))/(EUEV-ELEV)
                ELSE IF(L.GT.HSO2(IL).AND.L.LT.LSO2(IL)) THEN
                  WGT=(ZF(IL,L-1)-ZF(IL,L))/(EUEV-ELEV)
                ELSE IF(L.EQ.LSO2(IL)) THEN
                  WGT=(ZF(IL,L-1)-ELEV)/(EUEV-ELEV)
                ELSE
                  WGT=0.
                ENDIF
                XEMIS(IL,L,JT)=XEMIS(IL,L,JT)+WGT*ESEVROW(IL)*FACT*32.064/64.059
              ENDIF
            ENDDO
        ENDDO
      ENDIF
!
      RETURN
      END
