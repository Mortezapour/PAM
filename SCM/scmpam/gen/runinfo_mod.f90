module runinfo
  !-----------------------------------------------------------------------
  !     purpose:
  !     --------
  !     basic information about experiment and air properies.
  !
  !     history:
  !     --------
  !     * sep 19/2008 - k.vonsalzen   add mutli level info
  !     * aug 11/2006 - k.vonsalzen   new.
  !
  !-----------------------------------------------------------------------
  !
  implicit none
  !
  !-----------------------------------------------------------------------
  !     * general model information (name, levels, time step).
  !
  integer, parameter :: imaxl=50 !<
  type mprp
    character(len=imaxl) :: runname !<
    real :: dzsfc !<
    real :: dt !<
    real :: wup !<
    real :: lat !<
    real :: sdoy !<
    real :: nugalt !<
    real :: nugvd !<
    real :: taux !<
    real :: taut !<
    real :: tauq !<
    real :: taum !<
    real :: cdn !<
    real :: cdnn !<
    real :: zref !<
    integer :: plev !<
    integer :: ntstp !<
    integer :: pltfq !<
    integer :: trax !<
    integer :: trat !<
    integer :: traq !<
    integer :: trup !<
    integer :: case !<
    end type mprp
    type(mprp) :: modl
    namelist /modnml/ modl
    !
    !     * initial properties of the air.
    !
    integer, parameter :: npermx=100 !<
    type airprp
      real :: zhi !<
      real :: tempi !<
      real :: presi !<
      real :: sviclr !<
      real, dimension(npermx) :: svicld !<
      real, dimension(npermx) :: cldf !<
      real, dimension(npermx) :: dtg !<
      real, dimension(npermx) :: dta !<
      real, dimension(npermx) :: drv !<
      integer :: nper !<
      integer, dimension(npermx) :: lvbot !<
      integer, dimension(npermx) :: lvtop !<
      integer, dimension(npermx) :: peron !<
      integer, dimension(npermx) :: peroff !<
      integer, dimension(npermx) :: dtgon !<
      integer, dimension(npermx) :: dtgoff !<
    end type airprp
    type(airprp) :: air
    namelist /airnml/ air
    !
    !     * emissions .
    !
    type emiss
      real :: so2em !<
      real :: orgem !<
      real :: ohrow !<
      real :: o3row !<
      real :: flnd !<
      real :: focn !<
      real :: docem1 !<
      real :: docem2 !<
      real :: docem3 !<
      real :: docem4 !<
      real :: dbcem1 !<
      real :: dbcem2 !<
      real :: dbcem3 !<
      real :: dbcem4 !<
      real :: st02row !<
      real :: st03row !<
      real :: st04row !<
      real :: st06row !<
      real :: st13row !<
      real :: st14row !<
      real :: st15row !<
      real :: st16row !<
      real :: st17row !<
      real :: fcanrow1 !<
      real :: fcanrow2 !<
      real :: fcanrow3 !<
      real :: fcanrow4 !<
      real :: spotrow !<
      real :: smfrac !<
      real :: bsfrac !<
    end type emiss
    type(emiss) :: emis
    namelist /emisnml/ emis
    !
  end module runinfo
