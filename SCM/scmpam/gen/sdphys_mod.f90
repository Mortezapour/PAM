module sdphys
  !-----------------------------------------------------------------------
  !     purpose:
  !     --------
  !     basic phyiscal constants.
  !
  !     history:
  !     --------
  !     * aug 10/2006 - k.vonsalzen   new.
  !
  !-----------------------------------------------------------------------
  !
  implicit none
  !
  !     * basic physical constants.
  !
  real, parameter :: rhoh2o = 1.e+03 !< density of water (kg/m3)
  real, parameter :: wh2o   = 18.015e-03 !< molecular weight of h2o (kg/mol)
  real, parameter :: wa     = 28.97e-03 !< molecular weight of air (kg/mol)
  real, parameter :: rgasm  = 8.31441 !< molar gas constant (j/mol/k)
  real, parameter :: rgas   = 287.04 !< gas constant of dry air (j/kg/k)
  real, parameter :: rgasv  = 461.50  !< Ideal gas constant for water vapor \f$[J kg^{-1} K^{-1}]\f$
  real, parameter :: rgocp  = 2./7. !<
  real, parameter :: cpres  = rgas/rgocp ! heat capacity at constant pressure
  ! for dry air (j/kg/k)
  real, parameter :: eps1   = 0.622 !< ratio of gas constant for dry
  ! air over gas constant for vapour
  real, parameter :: grav   = 9.80616 !< gravitational contstant (m/s2)
  real, parameter :: rl     = 2.501e+06 !< latent heat of vapourization (j/kg)
  !
  !     * fitting parameters for calculation of water vapour saturation
  !     * mixing ratio.
  !
  real, parameter :: rw1    = 53.67957 !< fitting parameter for calculation of water vapour saturation
  real, parameter :: rw2    = -6743.769 !< fitting parameter for calculation of water vapour saturation
  real, parameter :: rw3    = -4.8451 !< fitting parameter for calculation of water vapour saturation
  real, parameter :: ri1    = 23.33086 !< fitting parameter for calculation of water vapour saturation
  real, parameter :: ri2    = -6111.72784 !< fitting parameter for calculation of water vapour saturation
  real, parameter :: ri3    = 0.15215 !< fitting parameter for calculation of water vapour saturation
  !
  real, parameter :: ppa    = 21.656 !<
  real, parameter :: ppb    = 5418. !<
  !
end module sdphys
