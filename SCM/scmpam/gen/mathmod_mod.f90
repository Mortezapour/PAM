!> \file
!> \brief Contains routines for basic math
!
!-----------------------------------------------------------------------
module mathmod
  !-----------------------------------------------------------------------
  !
contains
  !
  subroutine vexp(xout,xin,nin)
    !======================================================================= &
    ! vec or exponential. substitute by appropriate function on supercomputer.
    !=======================================================================
    use fpdef
    integer, intent(in) :: nin !<
    real, intent(inout)(r8), dimension(nin) :: xout !<
    real, intent(in)(r8), dimension(nin) :: xin !<
    !
    if (kdbl) then
      xout(1:nin) = exp(xin(1:nin))
    else
      xout(1:nin) = &! proc-depend
                      exp(xin(1:nin))
    end if
    !
  end subroutine vexp
  subroutine vlog(xout,xin,nin)
    !======================================================================= &
    ! vec or log. substitute by appropriate function on supercomputer.
    !=======================================================================
    use fpdef
    integer, intent(in) :: nin !<
    real, intent(inout)(r8), dimension(nin) :: xout !<
    real, intent(in)(r8), dimension(nin) :: xin !<
    !
    if (kdbl) then
      xout(1:nin) = log(xin(1:nin))
    else
      xout(1:nin) = &! proc-depend
                      log(xin(1:nin))
    end if
    !
  end subroutine vlog
  subroutine vsqrt(xout,xin,nin)
    !======================================================================= &
    ! vec or square root, substitute by appropriate function on supercomputer.
    !=======================================================================
    use fpdef
    integer, intent(in) :: nin !<
    real, intent(inout)(r8), dimension(nin) :: xout !<
    real, intent(in)(r8), dimension(nin) :: xin !<
    !
    if (kdbl) then
      xout(1:nin) = sqrt(xin(1:nin))
    else
      xout(1:nin) = &! proc-depend
                      sqrt(xin(1:nin))
    end if
    !
  end subroutine vsqrt
  subroutine vpow(xout,xin,yin,nin)
    !======================================================================= &
    ! vec or power. substitute by appropriate function on supercomputer.
    !=======================================================================
    use fpdef
    integer, intent(in) :: nin !<
    real(r8), intent(in), dimension(nin) :: xin !<
    real(r8), intent(in), dimension(nin) :: yin !<
    real(r8), intent(out), dimension(nin) :: xout !<
    !
    xout = xin**yin
    !
  end subroutine vpow
  subroutine vsexp(xout,xin,nin)
    !======================================================================= &
    ! vec or exponential. substitute by appropriate function on supercomputer.
    !=======================================================================
    integer, intent(in) :: nin !<
    real, intent(inout), dimension(nin) :: xout !<
    real, intent(in), dimension(nin) :: xin !<
    !
    xout(1:nin) = exp(xin(1:nin))
    !
  end subroutine vsexp
  subroutine vslog(xout,xin,nin)
    !======================================================================= &
    ! vec or log. substitute by appropriate function on supercomputer.
    !=======================================================================
    integer, intent(in) :: nin !<
    real, intent(inout), dimension(nin) :: xout !<
    real, intent(in), dimension(nin) :: xin !<
    !
    xout(1:nin) = log(xin(1:nin))
    !
  end subroutine vslog
  subroutine vssqrt(xout,xin,nin)
    !======================================================================= &
    ! vec or square root. substitute by appropriate function on supercomputer.
    !=======================================================================
    integer, intent(in) :: nin !<
    real, intent(inout), dimension(nin) :: xout !<
    real, intent(in), dimension(nin) :: xin !<
    !
    xout(1:nin) = sqrt(xin(1:nin))
    !
  end subroutine vssqrt
  subroutine vspow(xout,xin,yin,nin)
    !======================================================================= &
    ! vec or power. substitute by appropriate function on supercomputer.
    !=======================================================================
    integer, intent(in) :: nin !<
    real, intent(in), dimension(nin) :: xin !<
    real, intent(in), dimension(nin) :: yin !<
    real, intent(out), dimension(nin) :: xout !<
    !
    xout = xin**yin
    !
  end subroutine vspow
  !
end module mathmod
