module psizes
  !-----------------------------------------------------------------------
  !     purpose:
  !     --------
  !     basic array dimensions.
  !
  !     history:
  !     --------
  !     * feb 10/2010 - k.vonsalzen   new.
  !
  !-----------------------------------------------------------------------
  !
  implicit none
  !
  !-----------------------------------------------------------------------
  !     * number of grid points in horizontal direction
  integer :: ilg !<
  !     * vertical levels in driving atmospheric model.
  integer :: ilev !<
  integer :: ilevp1 !<
  integer :: levs !<
  !     * array dimensions for pla aerosol calculations
  integer :: msgt !<
  integer :: msgp1 !<
  integer :: msgp2 !<
  !     * tracers
  integer :: ntrac !<
  !     * number of distinct canopy types in class
  integer, parameter :: ican = 4 !<
  !
end module psizes
