module iodat
  !
  use fpdef
  !
  implicit none
  !
  integer, parameter :: imaxl=100 ! maximum length of variable long names
  integer, parameter :: itot=1000 ! maximum number of fields per file
  integer, parameter :: ismx=20 ! maximum number of dimensions for io fields
  !
  integer, parameter :: inax=-9 !<
  real(r4), parameter :: ynax=-9999. !<
  real(r4), parameter :: ysmallx=1.e-20 !<
  integer, dimension(ismx) :: idima !<
  character(len=imaxl) :: cna !<
  character(len=imaxl) :: ctmp !<
  type modprpv
    real, allocatable, dimension(:,:,:,:) :: fld4d !<
    real, allocatable, dimension(:,:,:) :: fld3d !<
    real, allocatable, dimension(:,:) :: fld2d !<
    real, allocatable, dimension(:) :: fld1d !<
    real :: fld0d !<
  end type modprpv
  type(modprpv), dimension(itot) :: ivarv,ovarv,rvarv
  type modprp
    character(len=imaxl) :: name !<
    character(len=imaxl) :: lname !<
    character(len=imaxl) :: units !<
    character(len=imaxl), dimension(ismx) :: dim !<
  end type modprp
  type(modprp), dimension(itot) :: ivar,ovar,rvar
  namelist /ivarnml/ ivar
  namelist /ovarnml/ ovar
  namelist /rvarnml/ rvar
  !
  integer :: ndim !<
  type dimprp
    character(len=imaxl) :: name !<
    character(len=imaxl) :: lname !<
    character(len=imaxl) :: units !<
    integer :: size !<
    integer :: flgi !<
    integer :: flgo !<
    integer :: flgrs !<
    integer :: flgx !<
    real, allocatable, dimension(:) :: val !<
  end type dimprp
  type(dimprp), allocatable, dimension(:) :: dim,dimt
  !
end module iodat
