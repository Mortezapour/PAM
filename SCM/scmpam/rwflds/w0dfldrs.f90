subroutine w0dfldrs(cname,avar)
  !
  use iodat
  !
  implicit none
  !
  integer :: ichck
  integer :: it
  !
  character(len=*), intent(in) :: cname !<
  real, intent(in) :: avar !<
  !
  ichck=0
  do it=1,itot
    if (abs(rvarv(it)%fld0d+ynax) > ysmallx &
        .and. trim(rvar(it)%name) == trim(cname) ) then
      ichck=1
      rvarv(it)%fld0d=avar
    end if
  end do
  if (ichck==0) then
    print*,'NO VARIABLE ',trim(cname), &
               ' IN LIST OF RESTART VARIABLES'
    call xit('W0DFLDRS',-1)
  end if
  !
end subroutine w0dfldrs
