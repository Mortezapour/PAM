subroutine w2dfld(cname,avar,idim1,idim2,j1,j2,k1,k2)
  !
  use iodat
  !
  implicit none
  !
  integer :: i1
  integer :: i2
  integer :: ichck
  integer :: it
  !
  character(len=*), intent(in) :: cname !<
  real, intent(in), dimension(idim1,idim2) :: avar !<
  integer, intent(in) :: idim1 !<
  integer, intent(in) :: idim2 !<
  integer, intent(in), optional :: j1 !<
  integer, intent(in), optional :: j2 !<
  integer, intent(in), optional :: k1 !<
  integer, intent(in), optional :: k2 !<
  integer :: j1t !<
  integer :: j2t !<
  integer :: k1t !<
  integer :: k2t !<
  integer, dimension(2) :: ishape !<
  !
  if (present(j1) .and. present(j2) &
      .and. present(k1) .and. present(k2) ) then
    if (j1 < 1 .or. k1 < 1 .or. j2 > idim1 .or. k2 > idim2) &
        call xit('W2DFLD',-1)
    j1t=j1
    j2t=j2
    k1t=k1
    k2t=k2
  else
    j1t=1
    j2t=idim1
    k1t=1
    k2t=idim2
  end if
  !
  ichck=0
  do it=1,itot
    if (allocated(ovarv(it)%fld2d) &
        .and. trim(ovar(it)%name) == trim(cname) ) then
      ichck=1
      !
      ishape=shape(ovarv(it)%fld2d)
      if (ishape(1) == idim1 .and. ishape(2) == idim2) then
        ovarv(it)%fld2d(j1t:j2t,k1t:k2t)=avar(j1t:j2t,k1t:k2t)
      else if (ishape(1) == idim2 .and. ishape(2) == idim1) then
        do i1=k1t,k2t
          do i2=j1t,j2t
            ovarv(it)%fld2d(i1,i2)=avar(i2,i1)
          end do
        end do
      else
        print*,'INCOMPATIBLE DIMENSIONS VARIABLE ',trim(cname)
        call xit('W2DFLD',-2)
      end if
    end if
  end do
  if (ichck==0) then
    print*,'NO VARIABLE ',trim(cname),' IN LIST OF IO VARIABLES'
    call xit('W2DFLD',-3)
  end if
  !      print '("W2DFLD: ", a10, "   [",i2,"x",i2,"]  [",i2,"-",i2,"]  [",i2,"-",i2,"]", es12.2)', &
  !       cname, idim1,idim2, j1t,j2t, k1t,k2t, sum(avar)/size(avar)
  !
end subroutine w2dfld
