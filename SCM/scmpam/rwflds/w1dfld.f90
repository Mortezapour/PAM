subroutine w1dfld(cname,avar,idim1,j1,j2)
  !
  use iodat
  !
  implicit none
  !
  integer :: ichck
  integer :: it
  !
  character(len=*), intent(in) :: cname !<
  real, intent(in), dimension(idim1) :: avar !<
  integer, intent(in) :: idim1 !<
  integer, intent(in), optional :: j1 !<
  integer, intent(in), optional :: j2 !<
  integer :: j1t !<
  integer :: j2t !<
  integer, dimension(1) :: ishape !<
  !
  if (present(j1) .and. present(j2) ) then
    if (j1 < 1 .or. j2 > idim1) call xit('W1DFLD',-1)
    j1t=j1
    j2t=j2
  else
    j1t=1
    j2t=idim1
  end if
  !
  ichck=0
  do it=1,itot
    if (allocated(ovarv(it)%fld1d) &
        .and. trim(ovar(it)%name) == trim(cname) ) then
      ichck=1
      !
      ishape=shape(ovarv(it)%fld1d)
      if (ishape(1) == idim1) then
        ovarv(it)%fld1d(j1t:j2t)=avar(j1t:j2t)
      else
        print*,'INCOMPATIBLE DIMENSIONS VARIABLE ',trim(cname)
        call xit('W1DFLD',-2)
      end if
    end if
  end do
  if (ichck==0) then
    print*,'NO VARIABLE ',trim(cname),' IN LIST OF IO VARIABLES'
    call xit('W1DFLD',-3)
  end if
  !      print '("W1DFLD: ", a10, "   [",i2,"]  [",i2,"-",i2,"]", es12.2)', CNAME,IDIM1,J1T,J2T, sum(avar)/size(avar)
  !
end subroutine w1dfld
