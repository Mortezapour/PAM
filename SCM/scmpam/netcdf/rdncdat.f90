subroutine rdncdat(file_name)
  !
  use iodat
  use netcdf
  !
  implicit none
  !
  integer :: nd !<
  integer :: it !<
  integer :: iv !<
  character(len=*), intent(in) :: file_name !<
  integer :: ncid !<
  integer :: fil_varid !<
  integer, dimension(ndim) :: lvl_dimid !<
  integer, dimension(ndim) :: lvl_varid !<
  integer, dimension(itot) :: var_varid !<
  !
  call check(nf90_open(trim(file_name)//'.nc', &
                 nf90_nowrite, ncid) )
  !
  !     * allocate memory.
  !
  allocate(dimt(ndim))
  !
  !     * read the dimensions.
  !
  do nd=1,ndim
    if (dim(nd)%flgi == 1) then
      call check(nf90_inq_dimid(ncid, trim(dim(nd)%name), &
                     lvl_dimid(nd)) )
      call check(nf90_inquire_dimension(ncid, lvl_dimid(nd), &
                     len=dimt(nd)%size) )
      if (dimt(nd)%size /= dim(nd)%size) then
        print*,'CONFLICTING FIELD DIMENSION ',trim(dim(nd)%name)
        call xit('RDNCDATRS',-2)
      end if
    end if
  end do
  !
  !      find and read fields.
  !
  iv=0
  do it=1,itot
    if (abs(ivarv(it)%fld0d+ynax) > ysmallx) then
      iv=iv+1
      call check(nf90_inq_varid(ncid,trim(ivar(it)%name), &
                     var_varid(iv)) )
      call check(nf90_get_var(ncid, var_varid(iv), &
                     ivarv(it)%fld0d) )
    else if (allocated(ivarv(it)%fld1d) ) then
      iv=iv+1
      call check(nf90_inq_varid(ncid,trim(ivar(it)%name), &
                     var_varid(iv)) )
      call check(nf90_get_var(ncid, var_varid(iv), &
                     ivarv(it)%fld1d) )
    else if (allocated(ivarv(it)%fld2d) ) then
      iv=iv+1
      call check(nf90_inq_varid(ncid,trim(ivar(it)%name), &
                     var_varid(iv)) )
      call check(nf90_get_var(ncid, var_varid(iv), &
                     ivarv(it)%fld2d) )
    else if (allocated(ivarv(it)%fld3d) ) then
      iv=iv+1
      call check(nf90_inq_varid(ncid,trim(ivar(it)%name), &
                     var_varid(iv)) )
      call check(nf90_get_var(ncid, var_varid(iv), &
                     ivarv(it)%fld3d) )
    else if (allocated(ivarv(it)%fld4d) ) then
      iv=iv+1
      call check(nf90_inq_varid(ncid,trim(ivar(it)%name), &
                     var_varid(iv)) )
      call check(nf90_get_var(ncid, var_varid(iv), &
                     ivarv(it)%fld4d) )
    end if
  end do
  !
  !     * deallocation.
  !
  deallocate(dimt)
  !
contains
  subroutine check(status)
    integer, intent (in) :: status !<

    if (status /= nf90_noerr) then
      print *, trim(nf90_strerror(status))
      stop 2
    end if
  end subroutine check
end subroutine rdncdat
