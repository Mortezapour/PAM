subroutine setncdimprepam(alev,alev1,atime,arddr,atrac,aextt,aintt, &
                                asain,anrmf,asdus, &
                                ilev,ilev1,itime,irddr,itrac,kextt,kintt, &
                                isaintt,nrmfld,isdust)
  !
  use iodat
  !
  implicit none
  integer :: i
  integer :: nd
  !
  integer, intent(in) :: ilev !<
  integer, intent(in) :: ilev1 !<
  integer, intent(in) :: itime !<
  integer, intent(in) :: irddr !<
  integer, intent(in) :: itrac !<
  integer, intent(in) :: kextt !<
  integer, intent(in) :: kintt !<
  integer, intent(in) :: isaintt !<
  integer, intent(in) :: nrmfld !<
  integer, intent(in) :: isdust !<
  real, intent(in), dimension(ilev)    :: alev !<
  real, intent(in), dimension(ilev1)   :: alev1 !<
  real, intent(in), dimension(itime)   :: atime !<
  real, intent(in), dimension(irddr)   :: arddr !<
  real, intent(in), dimension(itrac)   :: atrac !<
  real, intent(in), dimension(kextt)   :: aextt !<
  real, intent(in), dimension(kintt)   :: aintt !<
  real, intent(in), dimension(isaintt) :: asain !<
  real, intent(in), dimension(nrmfld)  :: anrmf !<
  real, intent(in), dimension(isdust)  :: asdus !<
  !
  !     * define netcdf dimensions and associate with dimensions
  !     * in the model.
  !
  ndim=10
  allocate(dim(ndim))
  dim(:)%flgi=0
  dim(:)%flgo=0
  dim(:)%flgrs=0
  do nd=1,ndim
    do i=1,imaxl
      dim(nd)%name(i:i)=' '
      dim(nd)%units(i:i)=' '
      dim(nd)%lname(i:i)=' '
    end do
  end do
  !
  dim(1)%name (1:12)='level       '
  dim(1)%lname(1:12)='Altitude    '
  dim(1)%units(1:12)='m           '
  dim(1)%size=ilev
  allocate(dim(1)%val(dim(1)%size))
  dim(1)%val(:)=alev(:)
  !
  dim(2)%name (1:12)='level1      '
  dim(2)%lname(1:35)='Altitude of grid cell interfaces   '
  dim(2)%units(1:12)='m           '
  dim(2)%size=ilev+1
  allocate(dim(2)%val(dim(2)%size))
  dim(2)%val(:)=alev1(:)
  !
  dim(3)%name (1:12)='time        '
  dim(3)%lname(1:12)='Model time  '
  dim(3)%units(1:12)='s           '
  dim(3)%size=itime
  allocate(dim(3)%val(dim(3)%size))
  dim(3)%val(:)=atime(:)
  !
  dim(4)%name (1:12)='raddry      '
  dim(4)%lname(1:12)='log10(R/R0) '
  dim(4)%units(1:12)='1           '
  dim(4)%size=irddr
  allocate(dim(4)%val(dim(4)%size))
  dim(4)%val(:)=arddr(:)
  !
  dim(5)%name (1:12)='ntrac       '
  dim(5)%lname(1:12)='Tracer index'
  dim(5)%units(1:12)='1           '
  dim(5)%size=itrac
  allocate(dim(5)%val(dim(5)%size))
  dim(5)%val(:)=atrac(:)
  !
  dim(6)%name (1:12)='kextt       '
  dim(6)%lname(1:30)='Externally mixed species index'
  dim(6)%units(1:12)='1           '
  dim(6)%size=kextt
  allocate(dim(6)%val(dim(6)%size))
  dim(6)%val(:)=aextt(:)
  !
  dim(7)%name (1:12)='kintt       '
  dim(7)%lname(1:30)='Internally mixed species index'
  dim(7)%units(1:12)='1           '
  dim(7)%size=kintt
  allocate(dim(7)%val(dim(7)%size))
  dim(7)%val(:)=aintt(:)
  !
  dim(8)%name (1:12)='isaintt     '
  dim(8)%lname(1:30)='Internally mixed section index'
  dim(8)%units(1:12)='1           '
  dim(8)%size=isaintt
  allocate(dim(8)%val(dim(8)%size))
  dim(8)%val(:)=asain(:)
  !
  dim(9)%name (1:12)='nrmfld      '
  dim(9)%lname(1:20)='Time level index    '
  dim(9)%units(1:12)='1           '
  dim(9)%size=nrmfld
  allocate(dim(9)%val(dim(9)%size))
  dim(9)%val(:)=anrmf(:)
  !
  dim(10)%name (1:12)='isdust       '
  dim(10)%lname(1:30)='Dust section index           '
  dim(10)%units(1:12)='1           '
  dim(10)%size=isdust
  allocate(dim(10)%val(dim(10)%size))
  dim(10)%val(:)=asdus(:)
  !
end subroutine setncdimprepam
