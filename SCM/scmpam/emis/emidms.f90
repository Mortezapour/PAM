subroutine emidms(ddmsdt,focn,dmso,gt,zspd,dp,ilg,ilev)
  !
  use sdphys
  !
  implicit none
  real :: alpha
  real :: amwr
  real :: gamma
  integer :: il
  integer, intent(in) :: ilev
  integer, intent(in) :: ilg
  integer :: ka
  real :: sstmax
  real :: zdmscon
  real :: zdmsemiss
  real :: zkw
  real :: zschmidt
  real :: zsst
  real :: zvdms
  !
  real, intent(in), dimension(ilg) :: focn !<
  real, intent(in), dimension(ilg) :: dmso !<
  real, intent(in), dimension(ilg) :: gt !<
  real, intent(in), dimension(ilg) :: zspd !<
  real, intent(in), dimension(ilg,ilev) :: dp !<
  real, intent(out), dimension(ilg) :: ddmsdt !<
  !
  !     * calculate dms emissions following liss+merlivat
  !     * dms seawater conc. from kettle et al.
  !     * limit zsst so formulation for schmidt number remains
  !     * positive.
  !
  sstmax=36.
  do il=1,ilg
    zdmscon=dmso(il)*focn(il)
    zsst=min(gt(il)-273.15,sstmax)
    !
    !       * schmidt number according to saltzman et al. (1993).
    !
    zschmidt=2674.0-147.12*zsst+3.726*zsst**2-0.038*zsst**3
    !
    !       * gas transfer velocity at schmidt number of 600
    !       * according to nigtingale et al. (2000).
    !
    zkw=0.222*zspd(il)**2+0.333*zspd(il)
    !
    !       * gas transfer velocity at current schmidt number, airside
    !       * transfer velocity, and final transfer velocity (mcgillis
    !       * et al., 2000).
    !
    zkw=zkw*sqrt(600./zschmidt)
    amwr=18.0153/62.13
    ka=659.*zspd(il)*sqrt(amwr)
    alpha=exp(3525./(zsst+273.15)-9.464)
    gamma=1./(1.+ka/(alpha*zkw))
    zvdms=zkw*(1.-gamma)
    !
    !       * dms flux in kg/m2/sec, converted from nanomol/ltr*cm/hour.
    !
    zdmsemiss=zdmscon*zvdms*32.064e-11/3600.
    !
    !       * nanomol/ltr*cm/hour --> kg-s/m**2/sec
    !
    ddmsdt(il)=zdmsemiss*grav/dp(il,ilev)
  end do
  !
end subroutine emidms
