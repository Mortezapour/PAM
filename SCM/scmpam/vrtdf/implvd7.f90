subroutine implvd7 (a,b,c, cl,x,xg, il1,il2,ilg,ilev,todt, &
                          tend,delsig,raus,work,vint)

  !     * dec 07/89 - d.verseghy: same as previous version implvd6
  !     *                         except for removal of update of
  !     *                         surface fluxes.

  !     * calculate tendencies due to vertical diffusion in hybrid model.
  !     * the scheme is implicit backward.
  !     * a,b,c are respectively the lower, main and upper diag.

  implicit none
  integer :: i
  integer, intent(in) :: il1
  integer, intent(in) :: il2
  integer, intent(in) :: ilev
  integer, intent(in) :: ilg
  integer :: l
  real, intent(in) :: todt

  real, intent(in)   :: a     (ilg,ilev) !<
  real, intent(in)   :: b     (ilg,ilev) !<
  real, intent(in)   :: c     (ilg,ilev) !<
  real, intent(inout)   :: x     (ilg,ilev) !<
  real, intent(in)   :: work  (ilg,ilev) !<
  real, intent(in)   :: cl    (ilg) !<
  real, intent(inout)   :: xg    (ilg) !<
  real, intent(inout)   :: vint(ilg) !<
  real, intent(inout)   :: tend  (ilg,ilev) !<
  real, intent(in)   :: delsig(ilg,ilev) !<
  real, intent(in)   :: raus  (ilg) !<
  !-----------------------------------------------------------------------
  !     * get x+ (in array tend) from x-. save x(i,ilev) in vint(i).

  do i=il1,il2
    vint(i)   = x(i,ilev)
    x(i,ilev) = x(i,ilev) +todt*cl(i)*xg(i)
  end do ! loop 100

  call vrossr(tend, a,b,c,x,work,ilg,il1,il2,ilev)

  do i=il1,il2
    x(i,ilev) = vint(i)
  end do ! loop 150

  !     * get tendency from x- and x+.

  do l=1,ilev
    do i=il1,il2
      tend(i,l) = (tend(i,l)-x(i,l))*(1./todt)
    end do
  end do ! loop 200

  !     * calculate vertical integral vint.

  do i=il1,il2
    vint(i) = 0.
  end do ! loop 300

  do l=1,ilev
    do i=il1,il2
      vint(i) = vint(i) +tend(i,l)*delsig(i,l)
    end do
  end do ! loop 400

  return
end
