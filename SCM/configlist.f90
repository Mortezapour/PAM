      program configlist
!
      implicit none
!
      character(len=1) ::buffer 
      logical :: karg
      character(len=6) :: cfilatm
      character(len=10) :: cfilaer
!
!     * command line argument. 1=atmosphere, 2=atmosphere+aerosol
! 
      call getarg(1,buffer)
      read(buffer,*) karg
      cfilatm='IOLIST'
      cfilaer='IOLIST_PAM'
      call reforml(cfilatm,cfilaer,karg)
!
      end program configlist
      subroutine reforml(cfilatm,cfilaer,karg)
!
      implicit none
!
      character(len=6), intent(in) :: cfilatm
      character(len=10), intent(in) :: cfilaer
      logical, intent(in) :: karg
      character(len=4), dimension(3,3) :: cpre
      character(len=18) :: clab
      integer, parameter :: ilarge=10000000
      character(len=100) :: cdum,cdumx
      integer :: i,is,ix,nf,nfx,ifn
      integer, dimension(3,3) :: ifil,ic
!
      open(10,file=cfilatm)
      do i=1,ilarge
        read(10,'(a100)',end=999) cdum
        ix=0
        if ( cdum(1:29) == '* MODEL BOUNDARY CONDITIONS *' ) then
          is=1
          nf=1
          ifil(is,nf)=20
          open(ifil(is,nf),file='OUTPUT_PRE',status='replace')
          write(ifil(is,nf),'(a8)') '&OVARNML'
          cpre(is,nf)='OVAR'
          ic(is,nf)=0
          nf=nf+1
          ifil(is,nf)=21
          open(ifil(is,nf),file='INPUT_MOD',status='replace')
          write(ifil(is,nf),'(a8)') '&IVARNML'
          cpre(is,nf)='IVAR'
          ic(is,nf)=0
          ix=1
        endif
        if ( cdum(1:16) == '* OUTPUT MODEL *' ) then
          is=2
          nf=1
          ifil(is,nf)=30
          open(ifil(is,nf),file='OUTPUT_MOD',status='replace')
          write(ifil(is,nf),'(a8)') '&OVARNML'
          cpre(is,nf)='OVAR'
          ic(is,nf)=0
          ix=1
        endif
        if ( cdum(1:17) == '* RESTART MODEL *' ) then
          is=3
          nf=1
          ifil(is,nf)=40
          open(ifil(is,nf),file='RESTART_PRE',status='replace')
          write(ifil(is,nf),'(a8)') '&RVARNML'
          cpre(is,nf)='RVAR'
          ic(is,nf)=1
          write(ifil(is,nf),'()')
          write(ifil(is,nf),'(a22)') '! Invariant parameters'         
          write(ifil(is,nf),'()')          
          cdumx='NAME=''tstep'''
          call labll(clab,ic(is,nf),cdumx)
          write(ifil(is,nf),clab) cpre(is,nf),'(',ic(is,nf),')%',trim(cdumx)
          cdumx='LNAME=''time step index'''
          call labll(clab,ic(is,nf),cdumx)
          write(ifil(is,nf),clab) cpre(is,nf),'(',ic(is,nf),')%',trim(cdumx)
          cdumx='UNITS=''1'''
          call labll(clab,ic(is,nf),cdumx)
          write(ifil(is,nf),clab) cpre(is,nf),'(',ic(is,nf),')%',trim(cdumx)
          nf=nf+1
          ifil(is,nf)=41
          open(ifil(is,nf),file='RESTART_MOD',status='replace')
          write(ifil(is,nf),'(a8)') '&RVARNML'
          cpre(is,nf)='RVAR'
          ic(is,nf)=1
          write(ifil(is,nf),'()')
          write(ifil(is,nf),'(a22)') '! Invariant parameters'         
          write(ifil(is,nf),'()')          
          cdumx='NAME=''tstep'''
          call labll(clab,ic(is,nf),cdumx)
          write(ifil(is,nf),clab) cpre(is,nf),'(',ic(is,nf),')%',trim(cdumx)
          cdumx='LNAME=''time step index'''
          call labll(clab,ic(is,nf),cdumx)
          write(ifil(is,nf),clab) cpre(is,nf),'(',ic(is,nf),')%',trim(cdumx)
          cdumx='UNITS=''1'''
          call labll(clab,ic(is,nf),cdumx)
          write(ifil(is,nf),clab) cpre(is,nf),'(',ic(is,nf),')%',trim(cdumx)
          ix=1
        endif
        if ( cdum(1:4) == 'NAME' .or. cdum(1:5) == 'LNAME'.or. cdum(1:5) == 'UNITS' .or. cdum(1:3) == 'DIM') then
          do nfx=1,nf
            if ( cdum(1:4) == 'NAME' ) ic(is,nfx)=ic(is,nfx)+1
            call labll(clab,ic(is,nfx),cdum)
            write(ifil(is,nfx),clab) cpre(is,nfx),'(',ic(is,nfx),')%',trim(cdum)
          enddo
          ix=1
        endif
        if ( ix == 0 ) then
          do nfx=1,nf
            ifn=ifil(is,nfx)
            call wrtp(ifn,cdum)
          enddo
        endif
      enddo
 999  continue
      close(10)
!
!     * additional variables for aerosols.
!
      if ( karg ) then
        open(10,file=cfilaer)
        do i=1,ilarge
          read(10,'(a100)',end=9999) cdum
          ix=0
          if ( cdum(1:29) == '* MODEL BOUNDARY CONDITIONS *' ) then
            is=1
            nf=1
            ifil(is,nf)=ifil(is,2)
            cpre(is,nf)=cpre(is,2)
            ic(is,nf)=ic(is,2)
            nf=nf+1
            ifil(is,nf)=22
            open(ifil(is,nf),file='OUTPUT_PRE_PAM',status='replace')
            write(ifil(is,nf),'(a8)') '&OVARNML'
            cpre(is,nf)='OVAR'
            ic(is,nf)=0
            ix=1
          endif
          if ( cdum(1:16) == '* OUTPUT MODEL *' ) then
            is=2
            nf=1
            ix=1
          endif
          if ( cdum(1:17) == '* RESTART MODEL *' ) then
            is=3
            nf=1
            ifil(is,nf)=ifil(is,2)
            cpre(is,nf)=cpre(is,2)
            ic(is,nf)=ic(is,2)
            nf=nf+1
            ifil(is,nf)=42
            open(ifil(is,nf),file='RESTART_PRE_PAM',status='replace')
            write(ifil(is,nf),'(a8)') '&RVARNML'
            cpre(is,nf)='RVAR'
            ic(is,nf)=1
            write(ifil(is,nf),'()')
            write(ifil(is,nf),'(a22)') '! Invariant parameters'         
            write(ifil(is,nf),'()')          
            cdumx='NAME=''tstep'''
            call labll(clab,ic(is,nf),cdumx)
            write(ifil(is,nf),clab) cpre(is,nf),'(',ic(is,nf),')%',trim(cdumx)
            cdumx='LNAME=''time step index'''
            call labll(clab,ic(is,nf),cdumx)
            write(ifil(is,nf),clab) cpre(is,nf),'(',ic(is,nf),')%',trim(cdumx)
            cdumx='UNITS=''1'''
            call labll(clab,ic(is,nf),cdumx)
            write(ifil(is,nf),clab) cpre(is,nf),'(',ic(is,nf),')%',trim(cdumx)
            ix=1
          endif
          if ( cdum(1:4) == 'NAME' .or. cdum(1:5) == 'LNAME'.or. cdum(1:5) == 'UNITS' .or. cdum(1:3) == 'DIM') then
            do nfx=1,nf
              if ( cdum(1:4) == 'NAME' ) ic(is,nfx)=ic(is,nfx)+1
              call labll(clab,ic(is,nfx),cdum)
              write(ifil(is,nfx),clab) cpre(is,nfx),'(',ic(is,nfx),')%',trim(cdum)
            enddo
            ix=1
          endif
          if ( ix == 0 ) then
            do nfx=1,nf
              ifn=ifil(is,nfx)
              call wrtp(ifn,cdum)
            enddo
          endif
        enddo
 9999   continue
!
!       * close files.
!
        close(10)
        write(22,'(a1)') '/'
        close(22)
        write(42,'(a1)') '/'
        close(42)
      endif
!
!     * close files.
!
      write(20,'(a1)') '/'
      write(21,'(a1)') '/'
      write(30,'(a1)') '/'
      write(40,'(a1)') '/'
      write(41,'(a1)') '/'
      close(20)
      close(21)
      close(30)
      close(40)
      close(41)
!
      end subroutine reforml
      subroutine wrtp(ifn,cdum)
!
      implicit none
!
      integer, intent(in) :: ifn
      character(len=100), intent(in) :: cdum
      character(len=6) :: clab
      integer :: ic
!
      ic=len_trim(cdum)
      if ( ic > 0 ) then
        if ( ic < 10 ) then
          write(clab,'(a2,i1,a1)') '(a',ic,')'
        else if ( ic < 100 ) then
          write(clab,'(a2,i2,a1)') '(a',ic,')'
        else if ( ic < 1000 ) then
          write(clab,'(a2,i3,a1)') '(a',ic,')'
        endif
        write(ifn,clab) trim(cdum)
      else
        write(ifn,'()')
      endif
!
      end subroutine wrtp
      subroutine labll(clab,ic,cdum)
!
      implicit none
!
      character(len=18), intent(out) :: clab
      integer, intent(in) :: ic
      character(len=100), intent(in) :: cdum
      integer :: itr
      character(len=5) :: clx
!
      itr=len_trim(cdum)
      if ( itr < 10 ) then
        write(clx,'(a1,i1,a1)') 'a',itr,')'
      else if ( itr < 100 ) then
        write(clx,'(a1,i2,a1)') 'a',itr,')'
      else if ( itr < 1000 ) then
        write(clx,'(a1,i3,a1)') 'a',itr,')'
      endif
      if ( ic < 10 ) then
        write(clab,'(a13,a5)') '(a4,a1,i1,a2,',clx
      else if ( ic < 100 ) then
        write(clab,'(a13,a5)') '(a4,a1,i2,a2,',clx
      else if ( ic < 1000 ) then
        write(clab,'(a13,a5)') '(a4,a1,i3,a2,',clx
      else if ( ic < 10000 ) then
        write(clab,'(a13,a5)') '(a4,a1,i4,a2,',clx
      endif
!
      end subroutine labll
      subroutine xit(name,n)
! 
!     * oct 01/92 - e.chan. (change stop 1 to stop)
!     * jun 10/91 - e.chan. (translate hollerith literals and 
!     *                      dimension strings) 
! 
!     * oct 10/78 - j.d.henderson.
!     * terminates a program by printing the program name and 
!     * a line across the page followed by a number n. 
! 
!     * n.ge.0 is for a normal end. the line is dashed. 
!     * normal ends terminate with   stop.
! 
!     * n.lt.0 is for an abnormal end. the line is dotted.
!     * if n is less than -100 the program simply terminates. 
!     * otherwise if n is less than zero the program aborts.
! 
      implicit none
!      
      integer :: i
      integer :: n
      character*(*) name
      character*8   name8, dash, star
! 
      data dash /'--------'/, star /'********'/ 
!---------------------------------------------------------------------
! 
      name8 = name
      if(n.ge.0) write(6,6010) dash,name8,(dash,i=1,9),n 
! 
      if(n.lt.0) write(6,6010) star,name8,(star,i=1,9),n 
! 
      if ( n.ge.0 .or. n.lt.-100 ) then
        stop 
      else
        call abort
      endif
! 
!---------------------------------------------------------------------
 6010 format('0',a8,'  end  ',a8,9a8,i8)
      end   
